package com.alacritystudios.cryptotrace.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A Retrofit rest client factory.
 */

public class ApiClient {

    private static Retrofit mRetrofit;

    public static Retrofit getClient(String baseUrl) {
        if(mRetrofit!= null && mRetrofit.baseUrl().url().toString().equals(baseUrl)) {
            return mRetrofit;
        } else {
            Retrofit.Builder builder = new Retrofit.Builder();
            builder.baseUrl(baseUrl);
            builder.addConverterFactory(GsonConverterFactory.create());
            mRetrofit = builder.build();
            return mRetrofit;
        }
    }
}
