package com.alacritystudios.cryptotrace.rest;

import com.alacritystudios.cryptotrace.models.CurrencyHistogramModel;
import com.alacritystudios.cryptotrace.models.CurrencyListWrapperModel;
import com.alacritystudios.cryptotrace.models.ExchangeModel;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Contains retrofit calls to be implemented.
 */

public interface ApiService {

    @GET("coinlist/")
    Call<CurrencyListWrapperModel> getCryptoCurrencyList();

    @GET("pricemultifull")
    Call<ResponseBody> getCryptoCurrencyDetails(@Query("fsyms") String fromSymbol, @Query("tsyms") String toSymbol);

    @GET("histominute")
    Call<CurrencyHistogramModel> getCryptoCurrencyVariationsPerMinute(@Query("fsym") String fromSymbol, @Query("tsym") String toSymbol, @Query("e") String marketName);

    @GET("histohour")
    Call<CurrencyHistogramModel> getCryptoCurrencyVariationsPerHour(@Query("fsym") String fromSymbol, @Query("tsym") String toSymbol, @Query("e") String marketName, @Query("limit") int limit);

    @GET("histoday")
    Call<CurrencyHistogramModel> getCryptoCurrencyVariationsPerDay(@Query("fsym") String fromSymbol, @Query("tsym") String toSymbol, @Query("e") String marketName, @Query("limit") int limit);

    @GET("all/exchanges")
    Call<Map<String, Map<String, List<String>>>> getExchangeList();
}