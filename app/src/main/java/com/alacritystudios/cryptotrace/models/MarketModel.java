package com.alacritystudios.cryptotrace.models;

/**
 * Stores details of markets.
 */

public class MarketModel {

    private String marketName;
    private String marketCode;


    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public String getMarketCode() {
        return marketCode;
    }

    public void setMarketCode(String marketCode) {
        this.marketCode = marketCode;
    }

    public MarketModel(String marketName, String marketCode) {
        this.marketName = marketName;
        this.marketCode = marketCode;
    }
}
