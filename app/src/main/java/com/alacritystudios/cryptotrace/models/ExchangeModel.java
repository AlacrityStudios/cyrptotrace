package com.alacritystudios.cryptotrace.models;

import com.google.gson.annotations.Expose;

import java.util.List;
import java.util.Map;

/**
 * Model to store exchange details.
 */

public class ExchangeModel {

    @Expose
    private Map<String, Map<String, List<String>>> exchangeMap;

    public Map<String, Map<String, List<String>>> getExchangeMap() {
        return exchangeMap;
    }

    public void setExchangeMap(Map<String, Map<String, List<String>>> exchangeMap) {
        this.exchangeMap = exchangeMap;
    }

    public class ExchangeCoinListModel {

        private Map<String, List<String>> currencyMap;

        public Map<String, List<String>> getCurrencyMap() {
            return currencyMap;
        }

        public void setCurrencyMap(Map<String, List<String>> currencyMap) {
            this.currencyMap = currencyMap;
        }
    }

    public class ExchangeCoinConversionListModel {

        List<String> currencyConversionList;

        public List<String> getCurrencyConversionList() {
            return currencyConversionList;
        }

        public void setCurrencyConversionList(List<String> currencyConversionList) {
            this.currencyConversionList = currencyConversionList;
        }
    }
}
