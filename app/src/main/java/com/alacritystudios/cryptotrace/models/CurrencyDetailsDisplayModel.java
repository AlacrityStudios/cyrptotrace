package com.alacritystudios.cryptotrace.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Stores all the display formatted details about a currency model.
 */

public class CurrencyDetailsDisplayModel {

    @SerializedName("TYPE")
    @Expose
    private String type;
    @SerializedName("MARKET")
    @Expose
    private String market;
    @SerializedName("FROMSYMBOL")
    @Expose
    private String fromSymbol;
    @SerializedName("TOSYMBOL")
    @Expose
    private String toSymbol;
    @SerializedName("FLAGS")
    @Expose
    private String flags;
    @SerializedName("PRICE")
    @Expose
    private String price;
    @SerializedName("LASTUPDATE")
    @Expose
    private String lastUpdate;
    @SerializedName("LASTVOLUME")
    @Expose
    private String lastVolume;
    @SerializedName("LASTVOLUMETO")
    @Expose
    private String lastVolumeTo;
    @SerializedName("LASTTRADEID")
    @Expose
    private Double lastTradeId;
    @SerializedName("VOLUME24HOUR")
    @Expose
    private String volume24Hour;
    @SerializedName("VOLUME24HOURTO")
    @Expose
    private String volume24HourTo;
    @SerializedName("OPEN24HOUR")
    @Expose
    private String open24Hour;
    @SerializedName("HIGH24HOUR")
    @Expose
    private String high24Hour;
    @SerializedName("LOW24HOUR")
    @Expose
    private String low24Hour;
    @SerializedName("LASTMARKET")
    @Expose
    private String lastMarket;
    @SerializedName("CHANGE24HOUR")
    @Expose
    private String change24Hour;
    @SerializedName("CHANGEPCT24HOUR")
    @Expose
    private String changePct24Hour;
    @SerializedName("SUPPLY")
    @Expose
    private String supply;
    @SerializedName("MKTCAP")
    @Expose
    private String mktCap;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getFromSymbol() {
        return fromSymbol;
    }

    public void setFromSymbol(String fromSymbol) {
        this.fromSymbol = fromSymbol;
    }

    public String getToSymbol() {
        return toSymbol;
    }

    public void setToSymbol(String toSymbol) {
        this.toSymbol = toSymbol;
    }

    public String getFlags() {
        return flags;
    }

    public void setFlags(String flags) {
        this.flags = flags;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastVolume() {
        return lastVolume;
    }

    public void setLastVolume(String lastVolume) {
        this.lastVolume = lastVolume;
    }

    public String getLastVolumeTo() {
        return lastVolumeTo;
    }

    public void setLastVolumeTo(String lastVolumeTo) {
        this.lastVolumeTo = lastVolumeTo;
    }

    public Double getLastTradeId() {
        return lastTradeId;
    }

    public void setLastTradeId(Double lastTradeId) {
        this.lastTradeId = lastTradeId;
    }

    public String getVolume24Hour() {
        return volume24Hour;
    }

    public void setVolume24Hour(String volume24Hour) {
        this.volume24Hour = volume24Hour;
    }

    public String getVolume24HourTo() {
        return volume24HourTo;
    }

    public void setVolume24HourTo(String volume24HourTo) {
        this.volume24HourTo = volume24HourTo;
    }

    public String getOpen24Hour() {
        return open24Hour;
    }

    public void setOpen24Hour(String open24Hour) {
        this.open24Hour = open24Hour;
    }

    public String getHigh24Hour() {
        return high24Hour;
    }

    public void setHigh24Hour(String high24Hour) {
        this.high24Hour = high24Hour;
    }

    public String getLow24Hour() {
        return low24Hour;
    }

    public void setLow24Hour(String low24Hour) {
        this.low24Hour = low24Hour;
    }

    public String getLastMarket() {
        return lastMarket;
    }

    public void setLastMarket(String lastMarket) {
        this.lastMarket = lastMarket;
    }

    public String getChange24Hour() {
        return change24Hour;
    }

    public void setChange24Hour(String change24Hour) {
        this.change24Hour = change24Hour;
    }

    public String getChangePct24Hour() {
        return changePct24Hour;
    }

    public void setChangePct24Hour(String changePct24Hour) {
        this.changePct24Hour = changePct24Hour;
    }

    public String getSupply() {
        return supply;
    }

    public void setSupply(String supply) {
        this.supply = supply;
    }

    public String getMktCap() {
        return mktCap;
    }

    public void setMktCap(String mktCap) {
        this.mktCap = mktCap;
    }
}
