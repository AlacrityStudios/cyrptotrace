package com.alacritystudios.cryptotrace.models;

/**
 * A model tos tore the local currency details.
 */

public class LocalCurrencyDatabaseModel {

    private String currencyName;
    private Float currencyValueAgainstUsd;

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public Float getCurrencyValueAgainstUsd() {
        return currencyValueAgainstUsd;
    }

    public void setCurrencyValueAgainstUsd(Float currencyValueAgainstUsd) {
        this.currencyValueAgainstUsd = currencyValueAgainstUsd;
    }
}
