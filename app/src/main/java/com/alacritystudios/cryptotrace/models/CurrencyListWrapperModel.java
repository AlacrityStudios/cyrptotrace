package com.alacritystudios.cryptotrace.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

/**
 * A model to represent coins list wrapper.
 */

public class CurrencyListWrapperModel {

    @SerializedName("Response")
    @Expose
    private String response;

    @SerializedName("Message")
    @Expose
    private String message;

    @SerializedName("BaseImageUrl")
    @Expose
    private String baseImageUrl;

    @SerializedName("BaseLinkUrl")
    @Expose
    private String baseLinkUrl;

    @SerializedName("Data")
    @Expose
    private Map<String, CurrencyModel> data;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBaseImageUrl() {
        return baseImageUrl;
    }

    public void setBaseImageUrl(String baseImageUrl) {
        this.baseImageUrl = baseImageUrl;
    }

    public String getBaseLinkUrl() {
        return baseLinkUrl;
    }

    public void setBaseLinkUrl(String baseLinkUrl) {
        this.baseLinkUrl = baseLinkUrl;
    }

    public Map<String, CurrencyModel> getData() {
        return data;
    }

    public void setData(Map<String, CurrencyModel> data) {
        this.data = data;
    }
}
