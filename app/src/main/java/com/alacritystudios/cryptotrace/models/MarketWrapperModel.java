package com.alacritystudios.cryptotrace.models;

import java.util.List;

/**
 * Stores lists of market information
 */

public class MarketWrapperModel {

    List<MarketModel> mMarketModelList;

    public List<MarketModel> getmMarketModelList() {
        return mMarketModelList;
    }

    public void setmMarketModelList(List<MarketModel> mMarketModelList) {
        this.mMarketModelList = mMarketModelList;
    }
}
