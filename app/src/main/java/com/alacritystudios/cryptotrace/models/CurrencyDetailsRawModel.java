package com.alacritystudios.cryptotrace.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Stores all the raw details about a currency model.
 */

public class CurrencyDetailsRawModel {

    @SerializedName("TYPE")
    @Expose
    private String type;
    @SerializedName("MARKET")
    @Expose
    private String market;
    @SerializedName("FROMSYMBOL")
    @Expose
    private String fromSymbol;
    @SerializedName("TOSYMBOL")
    @Expose
    private String toSymbol;
    @SerializedName("FLAGS")
    @Expose
    private String flags;
    @SerializedName("PRICE")
    @Expose
    private Double price;
    @SerializedName("LASTUPDATE")
    @Expose
    private Integer lastUpdate;
    @SerializedName("LASTVOLUME")
    @Expose
    private Double lastVolume;
    @SerializedName("LASTVOLUMETO")
    @Expose
    private Double lastVolumeTo;
    @SerializedName("LASTTRADEID")
    @Expose
    private Double lastTradeId;
    @SerializedName("VOLUME24HOUR")
    @Expose
    private Double volume24Hour;
    @SerializedName("VOLUME24HOURTO")
    @Expose
    private Double volume24HourTo;
    @SerializedName("OPEN24HOUR")
    @Expose
    private Double open24Hour;
    @SerializedName("HIGH24HOUR")
    @Expose
    private Double high24Hour;
    @SerializedName("LOW24HOUR")
    @Expose
    private Double low24Hour;
    @SerializedName("LASTMARKET")
    @Expose
    private String lastMarket;
    @SerializedName("CHANGE24HOUR")
    @Expose
    private Double change24Hour;
    @SerializedName("CHANGEPCT24HOUR")
    @Expose
    private Double changePct24Hour;
    @SerializedName("SUPPLY")
    @Expose
    private Double supply;
    @SerializedName("MKTCAP")
    @Expose
    private Double mktCap;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getFromSymbol() {
        return fromSymbol;
    }

    public void setFromSymbol(String fromSymbol) {
        this.fromSymbol = fromSymbol;
    }

    public String getToSymbol() {
        return toSymbol;
    }

    public void setToSymbol(String toSymbol) {
        this.toSymbol = toSymbol;
    }

    public String getFlags() {
        return flags;
    }

    public void setFlags(String flags) {
        this.flags = flags;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Integer lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Double getLastVolume() {
        return lastVolume;
    }

    public void setLastVolume(Double lastVolume) {
        this.lastVolume = lastVolume;
    }

    public Double getLastVolumeTo() {
        return lastVolumeTo;
    }

    public void setLastVolumeTo(Double lastVolumeTo) {
        this.lastVolumeTo = lastVolumeTo;
    }

    public Double getLastTradeId() {
        return lastTradeId;
    }

    public void setLastTradeId(Double lastTradeId) {
        this.lastTradeId = lastTradeId;
    }

    public Double getVolume24Hour() {
        return volume24Hour;
    }

    public void setVolume24Hour(Double volume24Hour) {
        this.volume24Hour = volume24Hour;
    }

    public Double getVolume24HourTo() {
        return volume24HourTo;
    }

    public void setVolume24HourTo(Double volume24HourTo) {
        this.volume24HourTo = volume24HourTo;
    }

    public Double getOpen24Hour() {
        return open24Hour;
    }

    public void setOpen24Hour(Double open24Hour) {
        this.open24Hour = open24Hour;
    }

    public Double getHigh24Hour() {
        return high24Hour;
    }

    public void setHigh24Hour(Double high24Hour) {
        this.high24Hour = high24Hour;
    }

    public Double getLow24Hour() {
        return low24Hour;
    }

    public void setLow24Hour(Double low24Hour) {
        this.low24Hour = low24Hour;
    }

    public String getLastMarket() {
        return lastMarket;
    }

    public void setLastMarket(String lastMarket) {
        this.lastMarket = lastMarket;
    }

    public Double getChange24Hour() {
        return change24Hour;
    }

    public void setChange24Hour(Double change24Hour) {
        this.change24Hour = change24Hour;
    }

    public Double getChangePct24Hour() {
        return changePct24Hour;
    }

    public void setChangePct24Hour(Double changePct24Hour) {
        this.changePct24Hour = changePct24Hour;
    }

    public Double getSupply() {
        return supply;
    }

    public void setSupply(Double supply) {
        this.supply = supply;
    }

    public Double getMktCap() {
        return mktCap;
    }

    public void setMktCap(Double mktCap) {
        this.mktCap = mktCap;
    }
}
