package com.alacritystudios.cryptotrace.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Stores data of price history of currencies.
 */

public class CurrencyHistogramModel {

    @SerializedName("Response")
    @Expose
    private String response;
    @SerializedName("Type")
    @Expose
    private Integer type;
    @SerializedName("Aggregated")
    @Expose
    private Boolean aggregated;
    @SerializedName("Data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("TimeTo")
    @Expose
    private Integer timeTo;
    @SerializedName("TimeFrom")
    @Expose
    private Integer timeFrom;
    @SerializedName("FirstValueInArray")
    @Expose
    private Boolean firstValueInArray;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Boolean getAggregated() {
        return aggregated;
    }

    public void setAggregated(Boolean aggregated) {
        this.aggregated = aggregated;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Integer getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(Integer timeTo) {
        this.timeTo = timeTo;
    }

    public Integer getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(Integer timeFrom) {
        this.timeFrom = timeFrom;
    }

    public Boolean getFirstValueInArray() {
        return firstValueInArray;
    }

    public void setFirstValueInArray(Boolean firstValueInArray) {
        this.firstValueInArray = firstValueInArray;
    }

    public class Datum {

        @SerializedName("time")
        @Expose
        private Integer time;
        @SerializedName("close")
        @Expose
        private Double close;
        @SerializedName("high")
        @Expose
        private Double high;
        @SerializedName("low")
        @Expose
        private Double low;
        @SerializedName("open")
        @Expose
        private Double open;
        @SerializedName("volumefrom")
        @Expose
        private Double volumefrom;
        @SerializedName("volumeto")
        @Expose
        private Double volumeto;

        public Integer getTime() {
            return time;
        }

        public void setTime(Integer time) {
            this.time = time;
        }

        public Double getClose() {
            return close;
        }

        public void setClose(Double close) {
            this.close = close;
        }

        public Double getHigh() {
            return high;
        }

        public void setHigh(Double high) {
            this.high = high;
        }

        public Double getLow() {
            return low;
        }

        public void setLow(Double low) {
            this.low = low;
        }

        public Double getOpen() {
            return open;
        }

        public void setOpen(Double open) {
            this.open = open;
        }

        public Double getVolumefrom() {
            return volumefrom;
        }

        public void setVolumefrom(Double volumefrom) {
            this.volumefrom = volumefrom;
        }

        public Double getVolumeto() {
            return volumeto;
        }

        public void setVolumeto(Double volumeto) {
            this.volumeto = volumeto;
        }

    }
}
