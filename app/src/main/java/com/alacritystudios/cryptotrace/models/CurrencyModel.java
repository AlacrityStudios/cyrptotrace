package com.alacritystudios.cryptotrace.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * A model to store crypto currency list details.
 */

public class CurrencyModel {

    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("CoinName")
    @Expose
    private String coinName;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("ImageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("SortOrder")
    @Expose
    private String sortOrder;
    @SerializedName("Sponsored")
    @Expose
    private Boolean isCryptoCurrency;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoinName() {
        return coinName;
    }

    public void setCoinName(String coinName) {
        this.coinName = coinName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Boolean getCryptoCurrency() {
        return isCryptoCurrency;
    }

    public void setCryptoCurrency(Boolean cryptoCurrency) {
        isCryptoCurrency = cryptoCurrency;
    }
}
