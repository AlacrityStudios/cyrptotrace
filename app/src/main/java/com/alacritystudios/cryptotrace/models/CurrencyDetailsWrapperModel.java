package com.alacritystudios.cryptotrace.models;

/**
 * A wrapper model containing currency details returned by the API.
 */

public class CurrencyDetailsWrapperModel {

    CurrencyDetailsRawModel mCurrencyDetailsRawModel;

    CurrencyDetailsDisplayModel mCurrencyDetailsDisplayModel;

    public CurrencyDetailsRawModel getmCurrencyDetailsRawModel() {
        return mCurrencyDetailsRawModel;
    }

    public void setmCurrencyDetailsRawModel(CurrencyDetailsRawModel mCurrencyDetailsRawModel) {
        this.mCurrencyDetailsRawModel = mCurrencyDetailsRawModel;
    }

    public CurrencyDetailsDisplayModel getmCurrencyDetailsDisplayModel() {
        return mCurrencyDetailsDisplayModel;
    }

    public void setmCurrencyDetailsDisplayModel(CurrencyDetailsDisplayModel mCurrencyDetailsDisplayModel) {
        this.mCurrencyDetailsDisplayModel = mCurrencyDetailsDisplayModel;
    }

    public CurrencyDetailsWrapperModel(CurrencyDetailsRawModel mCurrencyDetailsRawModel, CurrencyDetailsDisplayModel mCurrencyDetailsDisplayModel) {
        this.mCurrencyDetailsRawModel = mCurrencyDetailsRawModel;
        this.mCurrencyDetailsDisplayModel = mCurrencyDetailsDisplayModel;
    }
}

