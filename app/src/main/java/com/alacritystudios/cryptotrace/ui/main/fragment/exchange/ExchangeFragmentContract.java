package com.alacritystudios.cryptotrace.ui.main.fragment.exchange;

import com.alacritystudios.cryptotrace.models.CurrencyModel;
import com.alacritystudios.cryptotrace.models.ExchangeModel;
import com.alacritystudios.cryptotrace.ui.base.BasePresenter;
import com.alacritystudios.cryptotrace.ui.base.BaseView;

import java.util.List;
import java.util.Map;

/**
 * Defines a contract between the view and the presenter.
 */

public interface ExchangeFragmentContract {

    public interface View extends BaseView<Presenter> {

        void showLoadingIndicator();

        void hideLoadingIndicator();

        void showConnectionErrorMessage();

        void receiveResult(Map<String, Map<String, List<String>>> stringMapMap);

        boolean isActive();
    }

    public interface Presenter extends BasePresenter {

        void fetchExchangeList();
    }
}
