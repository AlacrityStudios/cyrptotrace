package com.alacritystudios.cryptotrace.ui.main.fragment.currencydetails;

import com.alacritystudios.cryptotrace.models.CurrencyDetailsWrapperModel;
import com.alacritystudios.cryptotrace.models.CurrencyHistogramModel;
import com.alacritystudios.cryptotrace.ui.main.MainActivityRepository;

import java.lang.ref.WeakReference;

/**
 * Listens to user actions from the UI ({@link CurrencyDetailsFragment}), retrieves the data and updates the
 * UI as required.
 */

public class CurrencyDetailsFragmentPresenter implements CurrencyDetailsFragmentContract.Presenter {

    private WeakReference<CurrencyDetailsFragmentContract.View> mViewWeakReference;
    private MainActivityRepository mMainActivityRepository;

    public CurrencyDetailsFragmentPresenter(CurrencyDetailsFragment mCurrencyDetailsFragment) {
        this.mViewWeakReference = new WeakReference<CurrencyDetailsFragmentContract.View>(mCurrencyDetailsFragment);
        this.mMainActivityRepository = MainActivityRepository.getInstance();
        this.mViewWeakReference.get().setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void fetchCurrencyPrice(String fromSymbol, String toSymbol) {
        if (mViewWeakReference.get() != null) {
            mViewWeakReference.get().showLoadingIndicator();
            mViewWeakReference.get().isPriceLoading(true);
        }

        mMainActivityRepository.fetchCurrencyDetails(fromSymbol, toSymbol, new CurrencyDetailsFragmentDataSource.CurrencyDetailsCallback() {
            @Override
            public void onCurrencyDetailsFetched(CurrencyDetailsWrapperModel currencyDetailsWrapperModel) {
                if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                    mViewWeakReference.get().receiveCurrencyPrice(currencyDetailsWrapperModel);
                    mViewWeakReference.get().isPriceLoading(false);
                    mViewWeakReference.get().hideLoadingIndicator();
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                    mViewWeakReference.get().onCurrencyPriceError();
                    mViewWeakReference.get().isPriceLoading(false);
                    mViewWeakReference.get().hideLoadingIndicator();
                }
            }
        });
    }

    @Override
    public void fetchCurrencyHistogramByMinute(String fromSymbol, String toSymbol, String market) {
        if (mViewWeakReference.get() != null) {
            mViewWeakReference.get().showLoadingIndicator();
            mViewWeakReference.get().isHistogramLoading(true);
        }

        mMainActivityRepository.fetchCurrencyDetailsHistogramByMinute(fromSymbol, toSymbol, market, new CurrencyDetailsFragmentDataSource.CurrencyDetailsHistogramByMinuteCallback() {

            @Override
            public void onCurrencyDetailsHistogramFetched(CurrencyHistogramModel currencyHistogramModel) {
                if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                    if (currencyHistogramModel != null && currencyHistogramModel.getData() != null && currencyHistogramModel.getData().size() > 0) {
                        mViewWeakReference.get().receiveCurrencyHistogram(currencyHistogramModel);
                    } else {
                        mViewWeakReference.get().onCurrencyHistogramError();
                    }
                    mViewWeakReference.get().isHistogramLoading(false);
                    mViewWeakReference.get().hideLoadingIndicator();
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                    mViewWeakReference.get().onCurrencyHistogramError();
                    mViewWeakReference.get().isHistogramLoading(false);
                    mViewWeakReference.get().hideLoadingIndicator();
                }
            }
        });
    }

    @Override
    public void fetchCurrencyHistogramByHour(String fromSymbol, String toSymbol, String market, int limit) {
        if (mViewWeakReference.get() != null) {
            mViewWeakReference.get().showLoadingIndicator();
            mViewWeakReference.get().isHistogramLoading(true);
        }

        mMainActivityRepository.fetchCurrencyDetailsHistogramByHour(fromSymbol, toSymbol, market, limit, new CurrencyDetailsFragmentDataSource.CurrencyDetailsHistogramByHourCallback() {

            @Override
            public void onCurrencyDetailsHistogramFetched(CurrencyHistogramModel currencyHistogramModel) {
                if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                    if (currencyHistogramModel != null && currencyHistogramModel.getData() != null && currencyHistogramModel.getData().size() > 0) {
                        mViewWeakReference.get().receiveCurrencyHistogram(currencyHistogramModel);
                    } else {
                        mViewWeakReference.get().onCurrencyHistogramError();
                    }
                    mViewWeakReference.get().isHistogramLoading(false);
                    mViewWeakReference.get().hideLoadingIndicator();
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                    mViewWeakReference.get().onCurrencyHistogramError();
                    mViewWeakReference.get().isHistogramLoading(false);
                    mViewWeakReference.get().hideLoadingIndicator();
                }
            }
        });
    }

    @Override
    public void fetchCurrencyHistogramByDay(String fromSymbol, String toSymbol, String market, int limit) {
        if (mViewWeakReference.get() != null) {
            mViewWeakReference.get().showLoadingIndicator();
            mViewWeakReference.get().isHistogramLoading(true);
        }

        mMainActivityRepository.fetchCurrencyDetailsHistogramByDay(fromSymbol, toSymbol, market, limit, new CurrencyDetailsFragmentDataSource.CurrencyDetailsHistogramByDayCallback() {

            @Override
            public void onCurrencyDetailsHistogramFetched(CurrencyHistogramModel currencyHistogramModel) {
                if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                    if (currencyHistogramModel != null && currencyHistogramModel.getData() != null && currencyHistogramModel.getData().size() > 0) {
                        mViewWeakReference.get().receiveCurrencyHistogram(currencyHistogramModel);
                    } else {
                        mViewWeakReference.get().onCurrencyHistogramError();
                    }
                    mViewWeakReference.get().isHistogramLoading(false);
                    mViewWeakReference.get().hideLoadingIndicator();
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                    mViewWeakReference.get().onCurrencyHistogramError();
                    mViewWeakReference.get().isHistogramLoading(false);
                    mViewWeakReference.get().hideLoadingIndicator();
                }
            }
        });
    }
}