package com.alacritystudios.cryptotrace.ui.base;

public interface BaseView<T> {

    void setPresenter(T presenter);

}
