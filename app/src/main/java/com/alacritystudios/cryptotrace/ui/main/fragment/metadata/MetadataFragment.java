package com.alacritystudios.cryptotrace.ui.main.fragment.metadata;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.utilities.PreferenceUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Fetch metadata and save into local db.
 */

public class MetadataFragment extends Fragment implements MetadataFragmentContract.View {

    View mFragmentView;
    @BindView(R.id.srl_metadata)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.pb_metadata)
    ProgressBar mContentLoadingProgressBar;
    @BindView(R.id.ll_content)
    LinearLayout mContentLinearLayout;
    @BindView(R.id.ll_error)
    LinearLayout mErrorLayout;
    @BindView(R.id.tv_error)
    TextView mErrorTextView;

    Unbinder unbinder;
    boolean mIsStopped = false;
    int successCounter = 0;
    MetadataFragmentContract.Presenter mPresenter;
    MetadataFragmentInterface mMetadataFragmentInterface;

    public static MetadataFragment getInstance() {
        MetadataFragment metadataFragment = new MetadataFragment();
        Bundle bundle = new Bundle();
        metadataFragment.setArguments(bundle);
        return metadataFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_metadata, container, false);
        unbinder = ButterKnife.bind(this, mFragmentView);
        setupSwipeRefreshLayout();
        setupErrorLayout();
        return mFragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mMetadataFragmentInterface = (MetadataFragmentInterface) context;
        } catch (ClassCastException e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mMetadataFragmentInterface = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        mIsStopped = true;
    }

    @Override
    public void setPresenter(MetadataFragmentContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showConnectionErrorMessage() {
        mSwipeRefreshLayout.setEnabled(true);
        mErrorLayout.setVisibility(View.VISIBLE);
        mContentLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void incrementSuccessCounter() {
        successCounter++;
        Log.e(getClass().getSimpleName(), "incrementSuccessCounter() Successcounter value - " + successCounter);
        if (successCounter >= 2) {
            mMetadataFragmentInterface.onMetadataUpdated();
        }
    }

    @Override
    public void saveCryptoCurrencyUpdateDate() {
        PreferenceUtils.setLastCryptoCurrencyUpdateDate(getActivity());
    }

    @Override
    public void saveLocalCurrencyUpdateDate() {
        PreferenceUtils.setLastLocalCurrencyUpdateDate(getActivity());
    }

    @Override
    public void saveMarketUpdateDate() {
        PreferenceUtils.setLastMarketUpdateDate(getActivity());
    }

    @Override
    public boolean isActive() {
        return isAdded() && !mIsStopped;
    }

    /**
     * Sets up the error layout.
     */
    private void setupErrorLayout() {
        mErrorTextView.setText(R.string.fetching_metadata_from_servers_error);
        mErrorLayout.setVisibility(View.GONE);
        mContentLinearLayout.setVisibility(View.VISIBLE);
    }

    /**
     * Sets up the swipe refresh layout.
     */
    private void setupSwipeRefreshLayout() {
        mSwipeRefreshLayout.setEnabled(false);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mContentLinearLayout.setVisibility(View.VISIBLE);
                mErrorLayout.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                successCounter = 0;
                mSwipeRefreshLayout.setEnabled(false);
                mPresenter.start();
            }
        });
    }

    public interface MetadataFragmentInterface {

        void onMetadataUpdated();
    }
}
