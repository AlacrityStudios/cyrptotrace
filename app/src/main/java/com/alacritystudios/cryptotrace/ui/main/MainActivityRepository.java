package com.alacritystudios.cryptotrace.ui.main;

import android.os.AsyncTask;
import android.util.Log;

import com.alacritystudios.cryptotrace.models.CurrencyDetailsDisplayModel;
import com.alacritystudios.cryptotrace.models.CurrencyDetailsRawModel;
import com.alacritystudios.cryptotrace.models.CurrencyDetailsWrapperModel;
import com.alacritystudios.cryptotrace.models.CurrencyHistogramModel;
import com.alacritystudios.cryptotrace.models.CurrencyListWrapperModel;
import com.alacritystudios.cryptotrace.models.CurrencyModel;
import com.alacritystudios.cryptotrace.models.ExchangeModel;
import com.alacritystudios.cryptotrace.models.MarketModel;
import com.alacritystudios.cryptotrace.models.MarketWrapperModel;
import com.alacritystudios.cryptotrace.rest.ApiClient;
import com.alacritystudios.cryptotrace.rest.ApiService;
import com.alacritystudios.cryptotrace.sql.CryptoWatchDatabaseHelper;
import com.alacritystudios.cryptotrace.ui.main.fragment.currencydetails.CurrencyDetailsFragmentDataSource;
import com.alacritystudios.cryptotrace.ui.main.fragment.currencylist.CurrencyListFragmentDataSource;
import com.alacritystudios.cryptotrace.ui.main.fragment.exchange.ExchangeFragmentDataSource;
import com.alacritystudios.cryptotrace.ui.main.fragment.marketlistdialog.MarketListDialogFragmentDataSource;
import com.alacritystudios.cryptotrace.ui.main.fragment.metadata.MetadataFragmentDataSource;
import com.alacritystudios.cryptotrace.utilities.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Concrete implementation to fetch data from the network.
 */

public class MainActivityRepository implements CurrencyListFragmentDataSource, CurrencyDetailsFragmentDataSource, MetadataFragmentDataSource, MarketListDialogFragmentDataSource, ExchangeFragmentDataSource {

    public static MainActivityRepository INSTANCE = null;
    private static final String TAG = MainActivityRepository.class.getSimpleName();

    public static MainActivityRepository getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MainActivityRepository();
        }
        return INSTANCE;
    }

    @Override
    public void fetchCryptoCurrencyListFromNetwork(final FetchCryptoCurrencyListNetworkCallback fetchCryptoCurrencyListNetworkCallback) {
        Retrofit retrofit = ApiClient.getClient(Utils.CRYPTO_COMPARE_BASE_URL);
        ApiService apiService = retrofit.create(ApiService.class);
        Call<CurrencyListWrapperModel> coinsListCall = apiService.getCryptoCurrencyList();
        coinsListCall.enqueue(new Callback<CurrencyListWrapperModel>() {
            @Override
            public void onResponse(Call<CurrencyListWrapperModel> call, Response<CurrencyListWrapperModel> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "apiService.getCryptoCurrencyList() - Response successful");
                    fetchCryptoCurrencyListNetworkCallback.onCurrencyListFetched(response.body());
                } else {
                    Log.d(TAG, "apiService.getCryptoCurrencyList() - Response unsuccessful");
                    fetchCryptoCurrencyListNetworkCallback.onDataNotAvailable();
                }
            }

            @Override
            public void onFailure(Call<CurrencyListWrapperModel> call, Throwable t) {
                Log.d(TAG, "apiService.getCryptoCurrencyList() - Response failure");
                fetchCryptoCurrencyListNetworkCallback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void fetchCurrencyDetails(final String fromSymbol, final String toSymbol, final CurrencyDetailsCallback currencyDetailsCallback) {
        Retrofit retrofit = ApiClient.getClient(Utils.CRYPTO_COMPARE_MINI_API_BASE_URL);
        ApiService apiService = retrofit.create(ApiService.class);
        Call<ResponseBody> coinsDetailsCall = apiService.getCryptoCurrencyDetails(fromSymbol, toSymbol);
        coinsDetailsCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        CurrencyDetailsRawModel currencyDetailsRawModel = gson.fromJson(jsonObject.getJSONObject("RAW").getJSONObject(fromSymbol).getJSONObject(toSymbol).toString(), CurrencyDetailsRawModel.class);
                        CurrencyDetailsDisplayModel currencyDetailsDisplayModel = gson.fromJson(jsonObject.getJSONObject("DISPLAY").getJSONObject(fromSymbol).getJSONObject(toSymbol).toString(), CurrencyDetailsDisplayModel.class);
                        currencyDetailsCallback.onCurrencyDetailsFetched(new CurrencyDetailsWrapperModel(currencyDetailsRawModel, currencyDetailsDisplayModel));
                    } catch (JSONException | IOException | NullPointerException e) {
                        e.printStackTrace();
                        currencyDetailsCallback.onDataNotAvailable();
                    }
                } else {
                    currencyDetailsCallback.onDataNotAvailable();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                currencyDetailsCallback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void fetchCryptoCurrencyListFromLocalCache(final WeakReference<CryptoWatchDatabaseHelper> cryptoWatchDatabaseHelper, final boolean isCryptoCurency, final FetchCryptoCurrencyListLocalCacheCallback fetchCryptoCurrencyListLocalCacheCallback) {
        new AsyncTask<Void, Void, List<CurrencyModel>>() {

            @Override
            protected void onPostExecute(List<CurrencyModel> currencyModelList) {

                if (currencyModelList != null) {
                    fetchCryptoCurrencyListLocalCacheCallback.onCurrencyListFetched(currencyModelList);
                } else {
                    fetchCryptoCurrencyListLocalCacheCallback.onDataNotAvailable();
                }
            }

            @Override
            protected List<CurrencyModel> doInBackground(Void... voids) {
                try {
                    if (cryptoWatchDatabaseHelper.get() != null) {
                        return cryptoWatchDatabaseHelper.get().fetchCurrencyDetails(isCryptoCurency);
                    } else {
                        return null;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    return null;
                }
            }
        }.execute();
    }

    @Override
    public void fetchMarketListFromLocalCache(final WeakReference<CryptoWatchDatabaseHelper> cryptoWatchDatabaseHelperWeakReference, final FetchMarketListLocalCacheCallback fetchMarketListLocalCacheCallback) {
        new AsyncTask<Void, Void, List<MarketModel>>() {

            @Override
            protected void onPostExecute(List<MarketModel> marketModelList) {

                if (marketModelList != null) {
                    fetchMarketListLocalCacheCallback.onMarketListFetched(marketModelList);
                } else {
                    fetchMarketListLocalCacheCallback.onDataNotAvailable();
                }
            }

            @Override
            protected List<MarketModel> doInBackground(Void... voids) {
                try {
                    if (cryptoWatchDatabaseHelperWeakReference.get() != null) {
                        return cryptoWatchDatabaseHelperWeakReference.get().fetchMarketDetails();
                    } else {
                        return null;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    return null;
                }
            }
        }.execute();
    }

    @Override
    public void saveCurrencyListIntoLocalCache(CurrencyListWrapperModel currencyListWrapperModel, final WeakReference<CryptoWatchDatabaseHelper> cryptoWatchDatabaseHelper, final boolean isCryptoCurency, final SaveCurrencyListIntoLocalCacheCallback saveCurrencyListIntoLocalCacheCallback) {
        new AsyncTask<CurrencyListWrapperModel, Void, Boolean>() {

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                if (aBoolean) {
                    saveCurrencyListIntoLocalCacheCallback.onSuccess();
                } else {
                    saveCurrencyListIntoLocalCacheCallback.onFailure();
                }
            }

            @Override
            protected Boolean doInBackground(CurrencyListWrapperModel... currencyListWrapperModels) {
                try {
                    if (cryptoWatchDatabaseHelper.get() != null) {
                        cryptoWatchDatabaseHelper.get().saveCurrencyDetails(new ArrayList<CurrencyModel>(currencyListWrapperModels[0].getData().values()), isCryptoCurency);
                        return true;
                    } else {
                        return false;
                    }
                } catch (Exception ex) {
                    Log.e(TAG, ex.getMessage());
                    return false;
                }
            }
        }.execute(currencyListWrapperModel);
    }

    @Override
    public void saveMarketListIntoLocalCache(final MarketWrapperModel marketWrapperModel, final WeakReference<CryptoWatchDatabaseHelper> cryptoWatchDatabaseHelperWeakReference, final SaveMarketListIntoLocalCacheCallback saveMarketListIntoLocalCacheCallback) {
        new AsyncTask<MarketWrapperModel, Void, Boolean>() {

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                if (aBoolean) {
                    saveMarketListIntoLocalCacheCallback.onSuccess();
                } else {
                    saveMarketListIntoLocalCacheCallback.onFailure();
                }
            }

            @Override
            protected Boolean doInBackground(MarketWrapperModel... marketWrapperModels) {
                try {
                    if (cryptoWatchDatabaseHelperWeakReference.get() != null) {
                        cryptoWatchDatabaseHelperWeakReference.get().saveMarketDetails(marketWrapperModel.getmMarketModelList());
                        return true;
                    } else {
                        return false;
                    }
                } catch (Exception ex) {
                    Log.e(TAG, ex.getMessage());
                    return false;
                }
            }
        }.execute(marketWrapperModel);
    }

    @Override
    public void fetchCurrencyDetailsHistogramByMinute(String fromSymbol, String toSymbol, String market, final CurrencyDetailsHistogramByMinuteCallback currencyDetailsHistogramByMinuteCallback) {
        Retrofit retrofit = ApiClient.getClient(Utils.CRYPTO_COMPARE_MINI_API_BASE_URL);
        ApiService apiService = retrofit.create(ApiService.class);
        Call<CurrencyHistogramModel> coinsDetailsCall = apiService.getCryptoCurrencyVariationsPerMinute(fromSymbol, toSymbol, market);
        coinsDetailsCall.enqueue(new Callback<CurrencyHistogramModel>() {
            @Override
            public void onResponse(Call<CurrencyHistogramModel> call, Response<CurrencyHistogramModel> response) {
                if (response.isSuccessful()) {
                    currencyDetailsHistogramByMinuteCallback.onCurrencyDetailsHistogramFetched(response.body());
                } else {
                    currencyDetailsHistogramByMinuteCallback.onDataNotAvailable();
                }
            }

            @Override
            public void onFailure(Call<CurrencyHistogramModel> call, Throwable t) {
                currencyDetailsHistogramByMinuteCallback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void fetchCurrencyDetailsHistogramByHour(String fromSymbol, String toSymbol, String market, int limit, final CurrencyDetailsHistogramByHourCallback currencyDetailsHistogramByHourCallback) {
        Retrofit retrofit = ApiClient.getClient(Utils.CRYPTO_COMPARE_MINI_API_BASE_URL);
        ApiService apiService = retrofit.create(ApiService.class);
        Call<CurrencyHistogramModel> coinsDetailsCall = apiService.getCryptoCurrencyVariationsPerHour(fromSymbol, toSymbol, market, limit);
        coinsDetailsCall.enqueue(new Callback<CurrencyHistogramModel>() {
            @Override
            public void onResponse(Call<CurrencyHistogramModel> call, Response<CurrencyHistogramModel> response) {
                if (response.isSuccessful()) {
                    currencyDetailsHistogramByHourCallback.onCurrencyDetailsHistogramFetched(response.body());
                } else {
                    currencyDetailsHistogramByHourCallback.onDataNotAvailable();
                }
            }

            @Override
            public void onFailure(Call<CurrencyHistogramModel> call, Throwable t) {
                currencyDetailsHistogramByHourCallback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void fetchCurrencyDetailsHistogramByDay(String fromSymbol, String toSymbol, String market, int limit, final CurrencyDetailsHistogramByDayCallback currencyDetailsHistogramByDayCallback) {
        Retrofit retrofit = ApiClient.getClient(Utils.CRYPTO_COMPARE_MINI_API_BASE_URL);
        ApiService apiService = retrofit.create(ApiService.class);
        Call<CurrencyHistogramModel> coinsDetailsCall = apiService.getCryptoCurrencyVariationsPerDay(fromSymbol, toSymbol, market, limit);
        coinsDetailsCall.enqueue(new Callback<CurrencyHistogramModel>() {
            @Override
            public void onResponse(Call<CurrencyHistogramModel> call, Response<CurrencyHistogramModel> response) {
                if (response.isSuccessful()) {
                    currencyDetailsHistogramByDayCallback.onCurrencyDetailsHistogramFetched(response.body());
                } else {
                    currencyDetailsHistogramByDayCallback.onDataNotAvailable();
                }
            }

            @Override
            public void onFailure(Call<CurrencyHistogramModel> call, Throwable t) {
                currencyDetailsHistogramByDayCallback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void fetchExchangeList(final FetchExchangeListCallback fetchExchangeListCallback) {
        Retrofit retrofit = ApiClient.getClient(Utils.CRYPTO_COMPARE_MINI_API_BASE_URL);
        ApiService apiService = retrofit.create(ApiService.class);
        Call<Map<String, Map<String, List<String>>>> exchangeListCall = apiService.getExchangeList();
        exchangeListCall.enqueue(new Callback<Map<String, Map<String, List<String>>>>() {
            @Override
            public void onResponse(Call<Map<String, Map<String, List<String>>>> call, Response<Map<String, Map<String, List<String>>>> response) {
                if (response.isSuccessful()) {
                    fetchExchangeListCallback.onExchangeListFetched(response.body());
                } else {
                    fetchExchangeListCallback.onDataNotAvailable();
                }
            }

            @Override
            public void onFailure(Call<Map<String, Map<String, List<String>>>> call, Throwable t) {
                fetchExchangeListCallback.onDataNotAvailable();
            }
        });
    }
}
