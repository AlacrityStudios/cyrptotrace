package com.alacritystudios.cryptotrace.ui.main.fragment.metadata;

import com.alacritystudios.cryptotrace.ui.base.BasePresenter;
import com.alacritystudios.cryptotrace.ui.base.BaseView;

/**
 * Establishes a contract between the view and the presenter.
 */

public interface MetadataFragmentContract {

    public interface View extends BaseView<MetadataFragmentContract.Presenter> {

        void showConnectionErrorMessage();

        void incrementSuccessCounter();

        void saveCryptoCurrencyUpdateDate();

        void saveLocalCurrencyUpdateDate();

        void saveMarketUpdateDate();

        boolean isActive();
    }

    public interface Presenter extends BasePresenter {

        void seedLocalCurrencyList();

        void seedCryptoCurrencyList();

        void seedMarketList();
    }
}
