package com.alacritystudios.cryptotrace.ui.main.fragment.calculator;

/**
 * Listens to user actions from the UI ({@link CalculatorFragment}), retrieves the data and updates the
 * UI as required.
 */

public class CalculatorFragmentPresenter implements CalculatorFragmentContract.Presenter {


    @Override
    public void start() {

    }

    @Override
    public void fetchCurrencyList() {

    }

    @Override
    public void fetchCurrencyPrice(String fromSymbol, String toSymbol) {

    }
}
