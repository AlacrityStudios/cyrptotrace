package com.alacritystudios.cryptotrace.ui.main.fragment.marketlistdialog;

import com.alacritystudios.cryptotrace.models.MarketModel;
import com.alacritystudios.cryptotrace.ui.base.BasePresenter;
import com.alacritystudios.cryptotrace.ui.base.BaseView;

import java.util.List;

/**
 * Defines a contract between the view and the presenter.
 */

public interface MarketListDialogFragmentContract {

    public interface View extends BaseView<MarketListDialogFragmentContract.Presenter> {

        void receiveResult(List<MarketModel> marketModelList);

        boolean isActive();
    }

    public interface Presenter extends BasePresenter {

        void fetchMarketList();
    }
}
