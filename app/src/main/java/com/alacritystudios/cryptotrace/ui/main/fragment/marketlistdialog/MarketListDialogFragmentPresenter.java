package com.alacritystudios.cryptotrace.ui.main.fragment.marketlistdialog;

import com.alacritystudios.cryptotrace.models.MarketModel;
import com.alacritystudios.cryptotrace.sql.CryptoWatchDatabaseHelper;
import com.alacritystudios.cryptotrace.ui.main.MainActivityRepository;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Listens to user actions from the UI ({@link MarketListDialogFragment}), retrieves the data and updates the
 * UI as required.
 */

public class MarketListDialogFragmentPresenter implements MarketListDialogFragmentContract.Presenter {

    private WeakReference<MarketListDialogFragmentContract.View> mViewWeakReference;
    private WeakReference<CryptoWatchDatabaseHelper> mCryptoWatchDatabaseHelperWeakReference;
    private MainActivityRepository mMainActivityRepository;

    public MarketListDialogFragmentPresenter(MarketListDialogFragment marketListDialogFragment, CryptoWatchDatabaseHelper cryptoWatchDatabaseHelper) {
        this.mViewWeakReference = new WeakReference<MarketListDialogFragmentContract.View>(marketListDialogFragment);
        this.mCryptoWatchDatabaseHelperWeakReference = new WeakReference<CryptoWatchDatabaseHelper>(cryptoWatchDatabaseHelper);
        this.mMainActivityRepository = MainActivityRepository.getInstance();
        this.mViewWeakReference.get().setPresenter(this);
    }


    @Override
    public void start() {

    }

    @Override
    public void fetchMarketList() {
        mMainActivityRepository.fetchMarketListFromLocalCache(mCryptoWatchDatabaseHelperWeakReference, new MarketListDialogFragmentDataSource.FetchMarketListLocalCacheCallback() {

            @Override
            public void onMarketListFetched(List<MarketModel> marketModelList) {
                if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                    mViewWeakReference.get().receiveResult(marketModelList);
                }
            }

            @Override
            public void onDataNotAvailable() {
            }
        });
    }
}
