package com.alacritystudios.cryptotrace.ui.main.fragment.metadata;

import android.util.Log;

import com.alacritystudios.cryptotrace.models.CurrencyModel;
import com.alacritystudios.cryptotrace.models.CurrencyListWrapperModel;
import com.alacritystudios.cryptotrace.models.MarketModel;
import com.alacritystudios.cryptotrace.models.MarketWrapperModel;
import com.alacritystudios.cryptotrace.sql.CryptoWatchDatabaseHelper;
import com.alacritystudios.cryptotrace.ui.main.MainActivityRepository;
import com.alacritystudios.cryptotrace.ui.main.fragment.currencylist.CurrencyListFragmentDataSource;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Listens to user actions from the UI ({@link MetadataFragment}), retrieves the data and updates the
 * UI as required.
 */

public class MetadataFragmentPresenter implements MetadataFragmentContract.Presenter {

    private Map<String, String> mLocalCurrencies;
    private WeakReference<MetadataFragmentContract.View> mViewWeakReference;
    private WeakReference<CryptoWatchDatabaseHelper> mCryptoWatchDatabaseHelperWeakReference;
    private MainActivityRepository mMainActivityRepository;

    private static final String TAG = MetadataFragmentPresenter.class.getSimpleName();

    public MetadataFragmentPresenter(MetadataFragmentContract.View mView, CryptoWatchDatabaseHelper cryptoWatchDatabaseHelper) {
        this.mViewWeakReference = new WeakReference<MetadataFragmentContract.View>(mView);
        this.mCryptoWatchDatabaseHelperWeakReference = new WeakReference<CryptoWatchDatabaseHelper>(cryptoWatchDatabaseHelper);
        mLocalCurrencies = new HashMap<>();
        this.mMainActivityRepository = MainActivityRepository.getInstance();
        this.mViewWeakReference.get().setPresenter(this);
    }

    @Override
    public void start() {
        seedLocalCurrencyList();
        //seedCryptoCurrencyList();
        seedMarketList();
    }

    @Override
    public void seedLocalCurrencyList() {
        Log.d(TAG, "seedLocalCurrencyList()");
        mLocalCurrencies.put("AED", "United Arab Emirates Dirham");
        mLocalCurrencies.put("ARS", "Argentine Peso");
        mLocalCurrencies.put("AUD", "Australian Dollar");
        mLocalCurrencies.put("BGN", "Bulgarian Lev");
        mLocalCurrencies.put("BND", "Brunei Dollar");
        mLocalCurrencies.put("BOB", "Bolivian Boliviano");
        mLocalCurrencies.put("BRL", "Brazilian Real");
        mLocalCurrencies.put("CAD", "Canadian Dollar");
        mLocalCurrencies.put("CHF", "Swiss Franc");
        mLocalCurrencies.put("CLP", "Chilean Peso");
        mLocalCurrencies.put("CNY", "Chinese Yuan Renminbi");
        mLocalCurrencies.put("COP", "Colombian Peso");
        mLocalCurrencies.put("CZK", "Czech Republic Koruna");
        mLocalCurrencies.put("DKK", "Danish Krone");
        mLocalCurrencies.put("EGP", "Egyptian Pound");
        mLocalCurrencies.put("EUR", "Euro");
        mLocalCurrencies.put("FJD", "Fijian Dollar");
        mLocalCurrencies.put("GBP", "British Pound Sterling");
        mLocalCurrencies.put("HKD", "Hong Kong Dollar");
        mLocalCurrencies.put("HRK", "Croatian Kuna");
        mLocalCurrencies.put("HUF", "Hungarian Forint");
        mLocalCurrencies.put("IDR", "Indonesian Rupiah");
        mLocalCurrencies.put("ILS", "Israeli New Sheqel");
        mLocalCurrencies.put("INR", "Indian Rupee");
        mLocalCurrencies.put("JPY", "Japanese Yen");
        mLocalCurrencies.put("KES", "Kenyan Shilling");
        mLocalCurrencies.put("KRW", "South Korean Won");
        mLocalCurrencies.put("LTL", "Lithuanian Litas");
        mLocalCurrencies.put("MAD", "Moroccan Dirham");
        mLocalCurrencies.put("MXN", "Mexican Peso");
        mLocalCurrencies.put("MYR", "Malaysian Ringgit");
        mLocalCurrencies.put("NOK", "Norwegian Krone");
        mLocalCurrencies.put("NZD", "New Zealand Dollar");
        mLocalCurrencies.put("PEN", "Peruvian Nuevo Sol");
        mLocalCurrencies.put("PHP", "Philippine Peso");
        mLocalCurrencies.put("PKR", "Pakistani Rupee");
        mLocalCurrencies.put("PLN", "Polish Zloty");
        mLocalCurrencies.put("RON", "Romanian Leu");
        mLocalCurrencies.put("RSD", "Serbian Dinar");
        mLocalCurrencies.put("RUB", "Russian Ruble");
        mLocalCurrencies.put("SAR", "Saudi Riyal");
        mLocalCurrencies.put("SEK", "Swedish Krona");
        mLocalCurrencies.put("SGD", "Singapore Dollar");
        mLocalCurrencies.put("THB", "Thai Baht");
        mLocalCurrencies.put("TRY", "Turkish Lira");
        mLocalCurrencies.put("TWD", "New Taiwan Dollar");
        mLocalCurrencies.put("UAH", "Ukrainian Hryvnia");
        mLocalCurrencies.put("USD", "US Dollar");
        mLocalCurrencies.put("VEF", "Venezuelan Bolí­var Fuerte");
        mLocalCurrencies.put("VND", "Vietnamese Dong");
        mLocalCurrencies.put("ZAR", "South African Rand");

        CurrencyListWrapperModel currencyListWrapperModel = new CurrencyListWrapperModel();
        Map<String, CurrencyModel> stringCryptoCurrencyModelMap = new HashMap<>();
        for (Map.Entry<String, String> entry : mLocalCurrencies.entrySet()) {
            CurrencyModel currencyModel = new CurrencyModel();
            currencyModel.setName(entry.getKey());
            currencyModel.setCoinName(entry.getValue());
            currencyModel.setFullName(entry.getValue() + " (" + entry.getKey() + ")");
            currencyModel.setImageUrl("");
            currencyModel.setSortOrder("0");
            currencyModel.setCryptoCurrency(false);
            stringCryptoCurrencyModelMap.put(entry.getKey(), currencyModel);
        }

        currencyListWrapperModel.setData(stringCryptoCurrencyModelMap);

        mMainActivityRepository.saveCurrencyListIntoLocalCache(currencyListWrapperModel, mCryptoWatchDatabaseHelperWeakReference, false, new MetadataFragmentDataSource.SaveCurrencyListIntoLocalCacheCallback() {
            @Override
            public void onSuccess() {
                if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                    mViewWeakReference.get().saveLocalCurrencyUpdateDate();
                    mViewWeakReference.get().incrementSuccessCounter();
                }
            }

            @Override
            public void onFailure() {
                mViewWeakReference.get().showConnectionErrorMessage();
            }
        });
    }

    @Override
    public void seedCryptoCurrencyList() {
        Log.d(TAG, "seedCryptoCurrencyList()");
        mMainActivityRepository.fetchCryptoCurrencyListFromNetwork(new CurrencyListFragmentDataSource.FetchCryptoCurrencyListNetworkCallback() {
            @Override
            public void onCurrencyListFetched(CurrencyListWrapperModel currencyListWrapperModel) {
                if (currencyListWrapperModel != null && currencyListWrapperModel.getData() != null) {
                    mMainActivityRepository.saveCurrencyListIntoLocalCache(currencyListWrapperModel, mCryptoWatchDatabaseHelperWeakReference, true, new MetadataFragmentDataSource.SaveCurrencyListIntoLocalCacheCallback() {
                        @Override
                        public void onSuccess() {
                            if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                                mViewWeakReference.get().saveCryptoCurrencyUpdateDate();
                                mViewWeakReference.get().incrementSuccessCounter();
                            }
                        }

                        @Override
                        public void onFailure() {
                            mViewWeakReference.get().showConnectionErrorMessage();
                        }
                    });
                } else {
                    if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                        mViewWeakReference.get().showConnectionErrorMessage();
                    }
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                    mViewWeakReference.get().showConnectionErrorMessage();
                }
            }
        });
    }

    @Override
    public void seedMarketList() {
        Log.d(TAG, "seedMarketList()");
        MarketWrapperModel marketWrapperModel = new MarketWrapperModel();
        List<MarketModel> marketModelList = new ArrayList<>();
        marketModelList.add(new MarketModel("Default(Guarantees results)", "CCCAGG"));
        marketModelList.add(new MarketModel("BTC38", "BTC38"));
        marketModelList.add(new MarketModel("BTCC", "BTCC"));
        marketModelList.add(new MarketModel("BTCE", "BTCE"));
        marketModelList.add(new MarketModel("BTER", "BTER"));
        marketModelList.add(new MarketModel("Bit2C", "Bit2C"));
        marketModelList.add(new MarketModel("Bitfinex", "Bitfinex"));
        marketModelList.add(new MarketModel("Bitstamp", "Bitstamp"));
        marketModelList.add(new MarketModel("Bittrex", "Bittrex"));
        marketModelList.add(new MarketModel("CCEDK", "CCEDK"));
        marketModelList.add(new MarketModel("Cexio", "Cexio"));
        marketModelList.add(new MarketModel("Coinbase", "Coinbase"));
        marketModelList.add(new MarketModel("Coinfloor", "Coinfloor"));
        marketModelList.add(new MarketModel("Coinse", "Coinse"));
        marketModelList.add(new MarketModel("Coinsetter", "Coinsetter"));
        marketModelList.add(new MarketModel("Cryptopia", "Cryptopia"));
        marketModelList.add(new MarketModel("Cryptsy", "Cryptsy"));
        marketModelList.add(new MarketModel("Gatecoin", "Gatecoin"));
        marketModelList.add(new MarketModel("Gemini", "Gemini"));
        marketModelList.add(new MarketModel("HitBTC", "HitBTC"));
        marketModelList.add(new MarketModel("Huobi", "Huobi"));
        marketModelList.add(new MarketModel("itBit", "itBit"));
        marketModelList.add(new MarketModel("Kraken", "Kraken"));
        marketModelList.add(new MarketModel("LakeBTC", "LakeBTC"));
        marketModelList.add(new MarketModel("LocalBitcoins", "LocalBitcoins"));
        marketModelList.add(new MarketModel("MonetaGo", "MonetaGo"));
        marketModelList.add(new MarketModel("OKCoin", "OKCoin"));
        marketModelList.add(new MarketModel("Poloniex", "Poloniex"));
        marketModelList.add(new MarketModel("Yacuna", "Yacuna"));
        marketModelList.add(new MarketModel("Yunbi", "Yunbi"));
        marketModelList.add(new MarketModel("Yobit", "Yobit"));
        marketModelList.add(new MarketModel("Korbit", "Korbit"));
        marketModelList.add(new MarketModel("BitBay", "BitBay"));
        marketModelList.add(new MarketModel("BTCMarkets", "BTCMarkets"));
        marketModelList.add(new MarketModel("QuadrigaCX", "QuadrigaCX"));
        marketModelList.add(new MarketModel("CoinCheck", "CoinCheck"));
        marketModelList.add(new MarketModel("BitSquare", "BitSquare"));
        marketModelList.add(new MarketModel("Vaultoro", "Vaultoro"));
        marketModelList.add(new MarketModel("MercadoBitcoin", "MercadoBitcoin"));
        marketModelList.add(new MarketModel("Unocoin", "Unocoin"));
        marketModelList.add(new MarketModel("Bitso", "Bitso"));
        marketModelList.add(new MarketModel("BTCXIndia", "BTCXIndia"));
        marketModelList.add(new MarketModel("Paymium", "Paymium"));
        marketModelList.add(new MarketModel("TheRockTrading", "TheRockTrading"));
        marketModelList.add(new MarketModel("bitFlyer", "bitFlyer"));
        marketModelList.add(new MarketModel("Quoine", "Quoine"));
        marketModelList.add(new MarketModel("Luno", "Luno"));
        marketModelList.add(new MarketModel("EtherDelta", "EtherDelta"));
        marketModelList.add(new MarketModel("Liqui", "Liqui"));
        marketModelList.add(new MarketModel("bitFlyerFX", "bitFlyerFX"));
        marketModelList.add(new MarketModel("BitMarket", "BitMarket"));
        marketModelList.add(new MarketModel("LiveCoin", "LiveCoin"));
        marketModelList.add(new MarketModel("Coinone", "Coinone"));
        marketModelList.add(new MarketModel("Tidex", "Tidex"));
        marketModelList.add(new MarketModel("Bleutrade", "Bleutrade"));
        marketModelList.add(new MarketModel("EthexIndia", "EthexIndia"));
        marketWrapperModel.setmMarketModelList(marketModelList);

        mMainActivityRepository.saveMarketListIntoLocalCache(marketWrapperModel, mCryptoWatchDatabaseHelperWeakReference, new MetadataFragmentDataSource.SaveMarketListIntoLocalCacheCallback() {
            @Override
            public void onSuccess() {
                if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                    mViewWeakReference.get().saveMarketUpdateDate();
                    mViewWeakReference.get().incrementSuccessCounter();
                }
            }

            @Override
            public void onFailure() {
                mViewWeakReference.get().showConnectionErrorMessage();
            }
        });
    }
}
