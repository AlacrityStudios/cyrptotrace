package com.alacritystudios.cryptotrace.ui.main.fragment.marketlistdialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapters.MarketListAdapter;
import com.alacritystudios.cryptotrace.models.MarketModel;
import com.alacritystudios.cryptotrace.utilities.PreferenceUtils;
import com.lapism.searchview.SearchView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Returns a dialog to select the markets.
 */

public class MarketListDialogFragment extends DialogFragment implements MarketListDialogFragmentContract.View, MarketListAdapter.MarketListAdapterListener {

    View mFragmentView;
    @BindView(R.id.sv_market_list)
    SearchView mSearchView;
    @BindView(R.id.rv_market_list)
    RecyclerView mRecyclerView;

    Unbinder unbinder;
    boolean mIsStopped = false;
    List<MarketModel> mAllMarketModels;
    List<MarketModel> mSearchedMarketModels;
    MarketListDialogFragmentContract.Presenter mPresenter;
    MarketListAdapter mMarketListAdapter;
    LinearLayoutManager mLinearLayoutManager;

    MarketListDialogFragmentInterface mMarketListDialogFragmentInterface;

    public static MarketListDialogFragment getInstance() {
        return new MarketListDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mFragmentView = inflater.inflate(R.layout.dialog_market_list, container, false);
        unbinder = ButterKnife.bind(this, mFragmentView);
        setupRecyclerView();
        setupSearchView();
        return mFragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mMarketListDialogFragmentInterface = (MarketListDialogFragmentInterface) context;
        } catch (ClassCastException e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mMarketListDialogFragmentInterface = null;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.fetchMarketList();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        mIsStopped = true;
    }

    @Override
    public void setPresenter(MarketListDialogFragmentContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void receiveResult(List<MarketModel> marketModelList) {
        mAllMarketModels = marketModelList;
        mSearchedMarketModels = new ArrayList<>();
        if (mSearchView.getTextOnly().length() > 0) {
            for (MarketModel marketModel : marketModelList) {
                if (marketModel.getMarketName().toLowerCase().contains(mSearchView.getTextOnly().toString().toLowerCase())) {
                    mSearchedMarketModels.add(marketModel);
                }
            }
        } else {
            mSearchedMarketModels = mAllMarketModels;
        }
        mMarketListAdapter.setmMarketModelList(mSearchedMarketModels);
        mMarketListAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean isActive() {
        return isAdded() && !mIsStopped;
    }

    @Override
    public void onItemClick(String marketCode) {
        mMarketListDialogFragmentInterface.onMarketListItemClick(marketCode);
    }

    /**
     * Sets up the search view.
     */
    private void setupSearchView() {
        LinearLayout linearLayout = mSearchView.findViewById(com.lapism.searchview.R.id.search_linearLayout);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics())));
        linearLayout.setPadding((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics()), 0, 0, 0);
        mSearchView.findViewById(com.lapism.searchview.R.id.search_imageView_arrow).setVisibility(View.GONE);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                mSearchedMarketModels = new ArrayList<>();
                if (mAllMarketModels != null && mAllMarketModels.size() > 0) {
                    for (MarketModel marketModel : mAllMarketModels) {
                        if (marketModel.getMarketName().toLowerCase().contains(mSearchView.getTextOnly().toString().toLowerCase())) {
                            mSearchedMarketModels.add(marketModel);
                        }
                    }
                }
                mMarketListAdapter.setmMarketModelList(mSearchedMarketModels);
                mMarketListAdapter.notifyDataSetChanged();
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
        });
    }

    /**
     * Initialises the recycler view and attaches a layout manager and an adapter to it to display data.
     */
    private void setupRecyclerView() {
        mMarketListAdapter = new MarketListAdapter(getActivity(), new ArrayList<MarketModel>(), PreferenceUtils.getMarketChoice(getContext()),this);
        mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setAdapter(mMarketListAdapter);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
    }


    public interface MarketListDialogFragmentInterface {

        void onMarketListItemClick(String marketCode);
    }
}
