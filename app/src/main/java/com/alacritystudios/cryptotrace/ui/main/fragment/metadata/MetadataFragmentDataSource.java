package com.alacritystudios.cryptotrace.ui.main.fragment.metadata;

import com.alacritystudios.cryptotrace.models.CurrencyListWrapperModel;
import com.alacritystudios.cryptotrace.models.MarketWrapperModel;
import com.alacritystudios.cryptotrace.sql.CryptoWatchDatabaseHelper;

import java.lang.ref.WeakReference;

/**
 * Main entry point for updating and fetching metadata.
 */

public interface MetadataFragmentDataSource {

    public interface SaveCurrencyListIntoLocalCacheCallback {

        void onSuccess();

        void onFailure();
    }

    public interface SaveMarketListIntoLocalCacheCallback {

        void onSuccess();

        void onFailure();
    }

    void saveCurrencyListIntoLocalCache(CurrencyListWrapperModel currencyListWrapperModel, WeakReference<CryptoWatchDatabaseHelper> cryptoWatchDatabaseHelperWeakReference,
                                        boolean isCryptoCurrency, SaveCurrencyListIntoLocalCacheCallback saveCurrencyListIntoLocalCacheCallback);

    void saveMarketListIntoLocalCache(MarketWrapperModel marketWrapperModel, WeakReference<CryptoWatchDatabaseHelper> cryptoWatchDatabaseHelperWeakReference,
                                        SaveMarketListIntoLocalCacheCallback saveMarketListIntoLocalCacheCallback);
}
