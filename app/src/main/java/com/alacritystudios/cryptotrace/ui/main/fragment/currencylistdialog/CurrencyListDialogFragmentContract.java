package com.alacritystudios.cryptotrace.ui.main.fragment.currencylistdialog;

import com.alacritystudios.cryptotrace.models.CurrencyModel;
import com.alacritystudios.cryptotrace.ui.base.BasePresenter;
import com.alacritystudios.cryptotrace.ui.base.BaseView;

import java.util.List;

/**
 * Defines a contract between the view and the presenter.
 */

public interface CurrencyListDialogFragmentContract {

    public interface View extends BaseView<CurrencyListDialogFragmentContract.Presenter> {

        void receiveResult(List<CurrencyModel> currencyModelList);

        boolean isActive();
    }

    public interface Presenter extends BasePresenter {

        void fetchCurrencyList(boolean isCryptoCurrency);
    }
}
