package com.alacritystudios.cryptotrace.ui.main.fragment.currencylistdialog;

import android.util.Log;

import com.alacritystudios.cryptotrace.models.CurrencyModel;
import com.alacritystudios.cryptotrace.sql.CryptoWatchDatabaseHelper;
import com.alacritystudios.cryptotrace.ui.main.MainActivityRepository;
import com.alacritystudios.cryptotrace.ui.main.fragment.currencylist.CurrencyListFragmentDataSource;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Listens to user actions from the UI ({@link CurrencyListDialogFragment}), retrieves the data and updates the
 * UI as required.
 */

public class CurrencyListDialogFragmentPresenter implements CurrencyListDialogFragmentContract.Presenter {

    private WeakReference<CurrencyListDialogFragmentContract.View> mViewWeakReference;
    private WeakReference<CryptoWatchDatabaseHelper> mCryptoWatchDatabaseHelperWeakReference;
    private MainActivityRepository mMainActivityRepository;

    public CurrencyListDialogFragmentPresenter(CurrencyListDialogFragment currencyListDialogFragment, CryptoWatchDatabaseHelper cryptoWatchDatabaseHelper) {
        this.mViewWeakReference = new WeakReference<CurrencyListDialogFragmentContract.View>(currencyListDialogFragment);
        this.mCryptoWatchDatabaseHelperWeakReference = new WeakReference<CryptoWatchDatabaseHelper>(cryptoWatchDatabaseHelper);
        this.mMainActivityRepository = MainActivityRepository.getInstance();
        this.mViewWeakReference.get().setPresenter(this);
    }


    @Override
    public void start() {

    }

    @Override
    public void fetchCurrencyList(boolean isCryptoCurrency) {
        mMainActivityRepository.fetchCryptoCurrencyListFromLocalCache(mCryptoWatchDatabaseHelperWeakReference, isCryptoCurrency, new CurrencyListFragmentDataSource.FetchCryptoCurrencyListLocalCacheCallback() {
            @Override
            public void onCurrencyListFetched(List<CurrencyModel> currencyModelList) {
                if(mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                    mViewWeakReference.get().receiveResult(currencyModelList);
                }
            }

            @Override
            public void onDataNotAvailable() {
            }
        });
    }
}
