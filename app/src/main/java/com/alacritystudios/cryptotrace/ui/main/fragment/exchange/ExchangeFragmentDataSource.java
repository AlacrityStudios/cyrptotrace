package com.alacritystudios.cryptotrace.ui.main.fragment.exchange;


import com.alacritystudios.cryptotrace.models.CurrencyListWrapperModel;
import com.alacritystudios.cryptotrace.models.CurrencyModel;
import com.alacritystudios.cryptotrace.models.ExchangeModel;
import com.alacritystudios.cryptotrace.sql.CryptoWatchDatabaseHelper;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;

/**
 * Main entry point for accessing currency list data.
 */

public interface ExchangeFragmentDataSource {

    public interface FetchExchangeListCallback {

        void onExchangeListFetched(Map<String, Map<String, List<String>>> stringMapMap);

        void onDataNotAvailable();
    }

    void fetchExchangeList(FetchExchangeListCallback fetchExchangeListCallback);
}
