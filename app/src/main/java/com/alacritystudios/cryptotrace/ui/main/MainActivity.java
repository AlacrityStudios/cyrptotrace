package com.alacritystudios.cryptotrace.ui.main;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapters.CryptoCurrencyListAdapter;
import com.alacritystudios.cryptotrace.adapters.ExchangeListAdapter;
import com.alacritystudios.cryptotrace.sql.CryptoWatchDatabaseHelper;
import com.alacritystudios.cryptotrace.ui.main.fragment.calculator.CalculatorFragment;
import com.alacritystudios.cryptotrace.ui.main.fragment.currencydetails.CurrencyDetailsFragment;
import com.alacritystudios.cryptotrace.ui.main.fragment.currencydetails.CurrencyDetailsFragmentPresenter;
import com.alacritystudios.cryptotrace.ui.main.fragment.currencylist.CurrencyListFragment;
import com.alacritystudios.cryptotrace.ui.main.fragment.currencylist.CurrencyListFragmentPresenter;
import com.alacritystudios.cryptotrace.ui.main.fragment.currencylistdialog.CurrencyListDialogFragment;
import com.alacritystudios.cryptotrace.ui.main.fragment.currencylistdialog.CurrencyListDialogFragmentPresenter;
import com.alacritystudios.cryptotrace.ui.main.fragment.exchange.ExchangeFragment;
import com.alacritystudios.cryptotrace.ui.main.fragment.exchange.ExchangeFragmentPresenter;
import com.alacritystudios.cryptotrace.ui.main.fragment.marketlistdialog.MarketListDialogFragment;
import com.alacritystudios.cryptotrace.ui.main.fragment.marketlistdialog.MarketListDialogFragmentPresenter;
import com.alacritystudios.cryptotrace.ui.main.fragment.metadata.MetadataFragment;
import com.alacritystudios.cryptotrace.ui.main.fragment.metadata.MetadataFragmentPresenter;
import com.alacritystudios.cryptotrace.ui.main.fragment.toolbar.ToolbarFragment;
import com.alacritystudios.cryptotrace.utilities.PreferenceUtils;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements CryptoCurrencyListAdapter.CoinsListAdapterListener, MetadataFragment.MetadataFragmentInterface,
        CurrencyListDialogFragment.CurrencyListDialogFragmentInterface, MarketListDialogFragment.MarketListDialogFragmentInterface, ExchangeListAdapter.ExchangeListAdapterListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(getClass().getSimpleName(), "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        Log.e(getClass().getSimpleName(), "onStart()");
        super.onStart();
        loadRelevantFragment();
    }

    @Override
    protected void onPause() {
        Log.e(getClass().getSimpleName(), "onPause()");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.e(getClass().getSimpleName(), "onStop()");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.e(getClass().getSimpleName(), "onDestroy()");
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.menu_calculator:
//                loadCalculatorFragment();
//                return true;
            case R.id.menu_attach_currency:
                loadCurrencyListDialogFragment();
                return true;
            case R.id.menu_select_market:
                loadMarketListDialogFragment();
                return true;
            case R.id.menu_reset_preferences:
                resetUserPreferences();
                return true;
        }
        return false;
    }

    /**
     * Loads the relevant fragment into the container.
     */
    private void loadRelevantFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            if (getSupportFragmentManager().findFragmentByTag("ExchangeFragment") != null) {
                ExchangeFragment exchangeFragment = (ExchangeFragment) getSupportFragmentManager().findFragmentByTag("ExchangeFragment");
                new ExchangeFragmentPresenter(exchangeFragment);
            }
            if (getSupportFragmentManager().findFragmentByTag("CurrencyListFragment") != null) {
                CurrencyListFragment currencyListFragment = (CurrencyListFragment) getSupportFragmentManager().findFragmentByTag("CurrencyListFragment");
                new CurrencyListFragmentPresenter(currencyListFragment, CryptoWatchDatabaseHelper.getInstance(getApplicationContext()));
            }
            if (getSupportFragmentManager().findFragmentByTag("MetadataFragment") != null) {
                MetadataFragment metadataFragment = (MetadataFragment) getSupportFragmentManager().findFragmentByTag("MetadataFragment");
                new MetadataFragmentPresenter(metadataFragment, CryptoWatchDatabaseHelper.getInstance(getApplicationContext()));
            }
            if (getSupportFragmentManager().findFragmentByTag("CurrencyDetailsFragment") != null) {
                CurrencyDetailsFragment currencyDetailsFragment = (CurrencyDetailsFragment) getSupportFragmentManager().findFragmentByTag("CurrencyDetailsFragment");
                new CurrencyDetailsFragmentPresenter(currencyDetailsFragment);
            }

            if (getSupportFragmentManager().findFragmentByTag("CurrencyListDialogFragment") != null) {
                CurrencyListDialogFragment currencyListDialogFragment = (CurrencyListDialogFragment) getSupportFragmentManager().findFragmentByTag("CurrencyListDialogFragment");
                new CurrencyListDialogFragmentPresenter(currencyListDialogFragment, CryptoWatchDatabaseHelper.getInstance(getApplicationContext()));
            }

            if (getSupportFragmentManager().findFragmentByTag("MarketListDialogFragment") != null) {
                MarketListDialogFragment marketListDialogFragment = (MarketListDialogFragment) getSupportFragmentManager().findFragmentByTag("MarketListDialogFragment");
                new MarketListDialogFragmentPresenter(marketListDialogFragment, CryptoWatchDatabaseHelper.getInstance(getApplicationContext()));
            }
        } else if ((System.currentTimeMillis() - PreferenceUtils.getLastLocalCurrencyUpdateDate(this) > TimeUnit.HOURS.toMillis(24)) ||
                (System.currentTimeMillis() - PreferenceUtils.getLastMarketUpdateDate(this) > TimeUnit.HOURS.toMillis(24))) {
            loadMetadataFragment();
        } else {
            loadToolbarFragment();
        }

    }

    /**
     * Loads the main toolbar fragment.
     */
    private void loadToolbarFragment() {
        Log.e(getClass().getSimpleName(), "loadToolbarFragment()");
        ToolbarFragment toolbarFragment;
        if (getSupportFragmentManager().findFragmentByTag("ToolbarFragment") != null) {
            toolbarFragment = (ToolbarFragment) getSupportFragmentManager().findFragmentByTag("ToolbarFragment");
        } else {
            toolbarFragment = ToolbarFragment.getInstance();
        }
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        fragmentTransaction.replace(R.id.fl_container, toolbarFragment, "ToolbarFragment");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    /**
     * Loads the currency list dialog fragment.
     */
    private void loadCurrencyListDialogFragment() {
        CurrencyListDialogFragment currencyListDialogFragment = CurrencyListDialogFragment.getInstance();
        new CurrencyListDialogFragmentPresenter(currencyListDialogFragment, CryptoWatchDatabaseHelper.getInstance(getApplicationContext()));
        currencyListDialogFragment.show(getSupportFragmentManager(), "CurrencyListDialogFragment");
    }

    /**
     * Loads the currency list dialog fragment.
     */
    private void loadMarketListDialogFragment() {
        MarketListDialogFragment marketListDialogFragment = MarketListDialogFragment.getInstance();
        new MarketListDialogFragmentPresenter(marketListDialogFragment, CryptoWatchDatabaseHelper.getInstance(getApplicationContext()));
        marketListDialogFragment.show(getSupportFragmentManager(), "MarketListDialogFragment");
    }

    /**
     * Loads the currency list fragment.
     */
    private void loadCurrencyListFragment() {
        Log.e(getClass().getSimpleName(), "loadCurrencyListFragment()");
        CurrencyListFragment currencyListFragment = CurrencyListFragment.getInstance();
        new CurrencyListFragmentPresenter(currencyListFragment, CryptoWatchDatabaseHelper.getInstance(getApplicationContext()));
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        fragmentTransaction.replace(R.id.fl_container, currencyListFragment, "CurrencyListFragment");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    /**
     * Loads the currency list fragment.
     */
    private void loadExchangeListFragment() {
        Log.e(getClass().getSimpleName(), "loadExchangeListFragment()");
        ExchangeFragment exchangeFragment = ExchangeFragment.getInstance();
        new ExchangeFragmentPresenter(exchangeFragment);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        fragmentTransaction.replace(R.id.fl_container, exchangeFragment, "ExchangeFragment");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    /**
     * Loads the metadata fragment.
     */
    private void loadMetadataFragment() {
        MetadataFragment metadataFragment = MetadataFragment.getInstance();
        new MetadataFragmentPresenter(metadataFragment, CryptoWatchDatabaseHelper.getInstance(getApplicationContext()));
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        fragmentTransaction.replace(R.id.fl_container, metadataFragment, "MetadataFragment");
        fragmentTransaction.commit();
    }

    /**
     * Loads the currency list fragment.
     */
    private void loadCalculatorFragment() {
        CalculatorFragment calculatorFragment = CalculatorFragment.getInstance();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        fragmentTransaction.add(R.id.fl_container, calculatorFragment);
        //new CurrencyListFragmentPresenter(currencyListFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    /**
     * Resets user preferences.
     */
    private void resetUserPreferences() {
        PreferenceUtils.setMarketChoice(this, "CCCAGG");
        PreferenceUtils.setConversionCurrencyChoice(this, "USD");
    }

    @Override
    public void onItemClick(String name) {

    }

    @Override
    public void onItemClick(String name, String fullName, String iconPath) {
        CurrencyDetailsFragment currencyDetailsFragment = CurrencyDetailsFragment.getInstance(name, fullName, iconPath);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        fragmentTransaction.add(R.id.fl_container, currencyDetailsFragment, "CurrencyDetailsFragment");
        new CurrencyDetailsFragmentPresenter(currencyDetailsFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onMetadataUpdated() {
        loadCurrencyListFragment();
    }

    @Override
    public void onCurrencyListItemClick(String currencyShortName) {
        PreferenceUtils.setConversionCurrencyChoice(this, currencyShortName);
        if (getSupportFragmentManager().findFragmentByTag("CurrencyListDialogFragment") != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.remove(getSupportFragmentManager().findFragmentByTag("CurrencyListDialogFragment"));
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onMarketListItemClick(String marketCode) {
        PreferenceUtils.setMarketChoice(this, marketCode);
        if (getSupportFragmentManager().findFragmentByTag("MarketListDialogFragment") != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.remove(getSupportFragmentManager().findFragmentByTag("MarketListDialogFragment"));
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            moveTaskToBack(true);
        }
    }
}
