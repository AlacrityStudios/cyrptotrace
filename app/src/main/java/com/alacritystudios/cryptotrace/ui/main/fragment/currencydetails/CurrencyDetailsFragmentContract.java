package com.alacritystudios.cryptotrace.ui.main.fragment.currencydetails;

import com.alacritystudios.cryptotrace.models.CurrencyDetailsWrapperModel;
import com.alacritystudios.cryptotrace.models.CurrencyHistogramModel;
import com.alacritystudios.cryptotrace.ui.base.BasePresenter;
import com.alacritystudios.cryptotrace.ui.base.BaseView;

/**
 * Defines a contract between the view and the presenter.
 */

public interface CurrencyDetailsFragmentContract {

    public interface View extends BaseView<CurrencyDetailsFragmentContract.Presenter> {

        void showLoadingIndicator();

        void hideLoadingIndicator();

        void showConnectionErrorMessage();

        void isPriceLoading(boolean isPriceLoading);

        void isHistogramLoading(boolean isHistogramLoading);

        void onCurrencyPriceError();

        void onCurrencyHistogramError();

        int getTabLayoutPosition();

        void receiveCurrencyPrice(CurrencyDetailsWrapperModel currencyDetailsWrapperModel);

        void receiveCurrencyHistogram(CurrencyHistogramModel currencyHistogramModel);

        boolean isActive();
    }

    public interface Presenter extends BasePresenter {

        void fetchCurrencyPrice(String fromSymbol, String toSymbol);

        void fetchCurrencyHistogramByMinute(String fromSymbol, String toSymbol, String market);

        void fetchCurrencyHistogramByHour(String fromSymbol, String toSymbol, String market, int limit);

        void fetchCurrencyHistogramByDay(String fromSymbol, String toSymbol, String market, int limit);
    }
}
