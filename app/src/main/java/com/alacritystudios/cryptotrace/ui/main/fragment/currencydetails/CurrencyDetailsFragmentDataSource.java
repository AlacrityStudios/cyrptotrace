package com.alacritystudios.cryptotrace.ui.main.fragment.currencydetails;

import com.alacritystudios.cryptotrace.models.CurrencyDetailsWrapperModel;
import com.alacritystudios.cryptotrace.models.CurrencyHistogramModel;

/**
 * Main entry point for accessing currency details data.
 */

public interface CurrencyDetailsFragmentDataSource {

    public interface CurrencyDetailsCallback {

        void onCurrencyDetailsFetched(CurrencyDetailsWrapperModel currencyDetailsWrapperModel);

        void onDataNotAvailable();
    }

    public interface CurrencyDetailsHistogramByMinuteCallback {

        void onCurrencyDetailsHistogramFetched(CurrencyHistogramModel currencyHistogramModel);

        void onDataNotAvailable();
    }

    public interface CurrencyDetailsHistogramByHourCallback {

        void onCurrencyDetailsHistogramFetched(CurrencyHistogramModel currencyHistogramModel);

        void onDataNotAvailable();
    }

    public interface CurrencyDetailsHistogramByDayCallback {

        void onCurrencyDetailsHistogramFetched(CurrencyHistogramModel currencyHistogramModel);

        void onDataNotAvailable();
    }

    void fetchCurrencyDetails(String fromSymbol, String toSymbol, CurrencyDetailsFragmentDataSource.CurrencyDetailsCallback currencyDetailsCallback);

    void fetchCurrencyDetailsHistogramByMinute(String fromSymbol, String toSymbol, String market, CurrencyDetailsFragmentDataSource.CurrencyDetailsHistogramByMinuteCallback currencyDetailsHistogramByMinuteCallback);

    void fetchCurrencyDetailsHistogramByHour(String fromSymbol, String toSymbol, String market, int limit, final CurrencyDetailsHistogramByHourCallback currencyDetailsHistogramByHourCallback);

    void fetchCurrencyDetailsHistogramByDay(String fromSymbol, String toSymbol, String market, int limit, final CurrencyDetailsHistogramByDayCallback currencyDetailsHistogramByDayCallback);
}
