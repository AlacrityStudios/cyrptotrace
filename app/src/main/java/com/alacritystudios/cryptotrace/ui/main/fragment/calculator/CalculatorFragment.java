package com.alacritystudios.cryptotrace.ui.main.fragment.calculator;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

/**
 * Convert from one currency to another.
 */

public class CalculatorFragment extends Fragment {

    View mFragmentView;
    AdView mAdView;
    Toolbar mToolbar;
    SwipeRefreshLayout mSwipeRefreshLayout;
    NestedScrollView mContentNestedScrollView;

    public static CalculatorFragment getInstance() {
        CalculatorFragment calculatorFragment = new CalculatorFragment();
        Bundle bundle = new Bundle();
        calculatorFragment.setArguments(bundle);
        return calculatorFragment;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
        }
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_calculator, container, false);
        assignUiWidgetsToMemberVariables();
        setupToolbar();
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        return mFragmentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /**
     * Assigns the UI widgets to member variables.
     */
    private void assignUiWidgetsToMemberVariables() {
        mAdView = mFragmentView.findViewById(R.id.av_calculator);
        mToolbar = mFragmentView.findViewById(R.id.toolbar);
        mSwipeRefreshLayout = mFragmentView.findViewById(R.id.srl_calculator);
        mContentNestedScrollView = mFragmentView.findViewById(R.id.nsv_content_layout);
    }

    /**
     * Sets up the toolbar.
     */
    private void setupToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
