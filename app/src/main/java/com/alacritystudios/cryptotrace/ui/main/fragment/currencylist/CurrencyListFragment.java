package com.alacritystudios.cryptotrace.ui.main.fragment.currencylist;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapters.CryptoCurrencyListAdapter;
import com.alacritystudios.cryptotrace.models.CurrencyModel;
import com.alacritystudios.cryptotrace.utilities.PreferenceUtils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.crash.FirebaseCrash;
import com.lapism.searchview.SearchView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A fragment to display a list of all crypto currencies.
 */

public class CurrencyListFragment extends Fragment implements CurrencyListFragmentContract.View {

    View mFragmentView;
    @BindView(R.id.av_coins_list)
    AdView mAdView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.sv_coins_list)
    SearchView mSearchView;
    @BindView(R.id.srl_coins_list)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.ll_no_records_found)
    LinearLayout mNoRecordsLayout;
    @BindView(R.id.rv_coins_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.ll_error)
    LinearLayout mErrorLayout;
    @BindView(R.id.tv_error)
    TextView mErrorTextView;
    LinearLayoutManager mLinearLayoutManager;
    CryptoCurrencyListAdapter mCryptoCurrencyListAdapter;
    List<CurrencyModel> mAllCurrencyModels;
    List<CurrencyModel> mSearchedCurrencyModels;

    Unbinder unbinder;
    boolean mIsStopped = false;
    CurrencyListFragmentContract.Presenter mPresenter;
    CryptoCurrencyListAdapter.CoinsListAdapterListener mCoinsListAdapterListener;

    public static CurrencyListFragment getInstance() {
        return new CurrencyListFragment();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_currency_list, menu);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.e(getClass().getSimpleName(), "onCreateView()");
        mFragmentView = inflater.inflate(R.layout.fragment_currency_list, container, false);
        unbinder = ButterKnife.bind(this, mFragmentView);
        setupAdView();
        setupToolbar();
        setupSearchView();
        setupSwipeRefreshLayout();
        setupRecyclerView();
        return mFragmentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.e(getClass().getSimpleName(), "onCreate()");
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        Log.e(getClass().getSimpleName(), "onAttach()");
        super.onAttach(context);
        if (context instanceof CryptoCurrencyListAdapter.CoinsListAdapterListener) {
            mCoinsListAdapterListener = (CryptoCurrencyListAdapter.CoinsListAdapterListener) context;
        }
    }

    @Override
    public void onStart() {
        Log.e(getClass().getSimpleName(), "onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.e(getClass().getSimpleName(), "onResume()");
        super.onResume();
        if(mPresenter != null) {
            mPresenter.start();
        } else {
            Log.e(getClass().getSimpleName(), "Presenter is null");
            FirebaseCrash.report(new Throwable("Presenter null."));
        }
    }

    @Override
    public void onDetach() {
        Log.e(getClass().getSimpleName(), "onDetach()");
        super.onDetach();
        mCoinsListAdapterListener = null;
    }

    @Override
    public void onDestroyView() {
        Log.e(getClass().getSimpleName(), "onDestroyView()");
        super.onDestroyView();
        unbinder.unbind();
        mIsStopped = true;
    }

    @Override
    public void onDestroy() {
        Log.e(getClass().getSimpleName(), "onDestroy()");
        super.onDestroy();
    }

    /**
     * Sets up the toolbar.
     */
    private void setupToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
    }

    /**
     * Sets up the toolbar.
     */
    private void setupAdView() {
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    /**
     * Sets up the search view.
     */
    private void setupSearchView() {
        LinearLayout linearLayout = mSearchView.findViewById(com.lapism.searchview.R.id.search_linearLayout);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics())));
        linearLayout.setPadding((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics()), 0, 0, 0);
        mSearchView.findViewById(com.lapism.searchview.R.id.search_imageView_arrow).setVisibility(View.GONE);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                mSearchedCurrencyModels = new ArrayList<>();
                if (mAllCurrencyModels != null && mAllCurrencyModels.size() > 0) {
                    for (CurrencyModel currencyModel : mAllCurrencyModels) {
                        if (currencyModel.getCoinName().toLowerCase().contains(mSearchView.getTextOnly().toString().toLowerCase())) {
                            mSearchedCurrencyModels.add(currencyModel);
                        }
                    }
                }
                mCryptoCurrencyListAdapter.setmCurrencyModels(mSearchedCurrencyModels);
                mCryptoCurrencyListAdapter.notifyDataSetChanged();
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
        });
    }

    /**
     * Initialises the recycler view and attaches a layout manager and an adapter to it to display data.
     */
    private void setupRecyclerView() {
        mCryptoCurrencyListAdapter = new CryptoCurrencyListAdapter(getActivity(), new ArrayList<CurrencyModel>(), mCoinsListAdapterListener);
        mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setAdapter(mCryptoCurrencyListAdapter);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
    }

    private void setupSwipeRefreshLayout() {
        mErrorTextView.setText(getString(R.string.currency_list_error));
        mErrorLayout.setVisibility(View.GONE);
        mNoRecordsLayout.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.start();
            }
        });
    }

    @Override
    public void setPresenter(CurrencyListFragmentContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showLoadingIndicator() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoadingIndicator() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showPlaceholderMessage() {
        mErrorLayout.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
        mNoRecordsLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showConnectionErrorMessage() {
        mNoRecordsLayout.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
        mErrorLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public long fetchLastCurrencyUpdateDate() {
        return PreferenceUtils.getLastCryptoCurrencyUpdateDate(getActivity());
    }

    @Override
    public void saveCurrencyUpdateDate() {
        PreferenceUtils.setLastCryptoCurrencyUpdateDate(getActivity());
    }

    @Override
    public void receiveResult(List<CurrencyModel> currencyModelList) {
        mAllCurrencyModels = currencyModelList;
        mSearchedCurrencyModels = new ArrayList<>();
        Collections.sort(currencyModelList, new Comparator<CurrencyModel>() {
            @Override
            public int compare(CurrencyModel t0, CurrencyModel t1) {
                if (Integer.parseInt(t0.getSortOrder()) > Integer.parseInt(t1.getSortOrder()))
                    return 1;
                else
                    return -1;
            }
        });
        if (mSearchView.getTextOnly().length() > 0) {
            for (CurrencyModel currencyModel : currencyModelList) {
                if (currencyModel.getCoinName().toLowerCase().contains(mSearchView.getTextOnly().toString().toLowerCase())) {
                    mSearchedCurrencyModels.add(currencyModel);
                }
            }
        } else {
            mSearchedCurrencyModels = mAllCurrencyModels;
        }
        mCryptoCurrencyListAdapter.setmCurrencyModels(mSearchedCurrencyModels);
        mCryptoCurrencyListAdapter.notifyDataSetChanged();
        mErrorLayout.setVisibility(View.GONE);
        mNoRecordsLayout.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean isActive() {
        return isAdded() && !mIsStopped;
    }
}
