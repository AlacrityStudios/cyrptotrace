package com.alacritystudios.cryptotrace.ui.main.fragment.currencydetails;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.models.CurrencyDetailsWrapperModel;
import com.alacritystudios.cryptotrace.models.CurrencyHistogramModel;
import com.alacritystudios.cryptotrace.utilities.PreferenceUtils;
import com.alacritystudios.cryptotrace.utilities.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Holds a summary of the currency details.
 */

public class CurrencyDetailsFragment extends Fragment implements CurrencyDetailsFragmentContract.View {

    View mFragmentView;
    @BindView(R.id.av_coins_details)
    AdView mAdView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.iv_coin_icon)
    ImageView mCurrencyIcon;
    @BindView(R.id.tv_currency_full_name)
    TextView mCurrencyFullName;
    @BindView(R.id.ll_error)
    LinearLayout mErrorLayout;
    @BindView(R.id.srl_coin_details)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.nsv_content_layout)
    NestedScrollView mContentNestedScrollView;
    @BindView(R.id.tl_currency_details)
    TabLayout mTabLayout;
    @BindView(R.id.tv_currency_to_type)
    TextView mCurrencyToTypeTextView;
    @BindView(R.id.tv_currency_rate)
    TextView mCurrencyRateTextView;
    @BindView(R.id.tv_currency_delta_summary)
    TextView mCurrencyDeltaSummaryTextView;
    @BindView(R.id.tv_open_24_hours)
    TextView mOpen24HoursTextView;
    @BindView(R.id.tv_market_cap)
    TextView mMarketCapTextView;
    @BindView(R.id.tv_high_24_hours)
    TextView mHigh24HoursTextView;
    @BindView(R.id.tv_low_24_hours)
    TextView mLow24HoursTextView;
    @BindView(R.id.iv_currency_delta_direction)
    ImageView mCurrencyDeltaImageView;
    @BindView(R.id.tv_from_symbol_name)
    TextView mFromSymbolNameTextView;
    @BindView(R.id.tv_to_symbol_name)
    TextView mToSymbolNameTextView;
    @BindView(R.id.lc_currency_histogram)
    LineChart mCurrencyHistogramLineChart;
    @BindView(R.id.et_from_symbol_computed_value)
    EditText mFromSymbolValueEditText;
    @BindView(R.id.tv_to_symbol_computed_value)
    TextView mToSymbolValueTextView;
    @BindView(R.id.ll_invert_search)
    LinearLayout mInvertSymbolConversionLinearLayout;


    boolean mIsStopped = false;
    Unbinder unbinder;
    CurrencyDetailsFragmentContract.Presenter mPresenter;

    String name;
    String fullName;
    String iconPath;
    String fromSymbol;
    String toSymbol;
    boolean isPriceLoading = false;
    boolean isHistogramLoading = false;
    Double toSymbolRate = 0.0;
    boolean isConversionInverted = false;

    public static CurrencyDetailsFragment getInstance(String name, String fullName, String iconPath) {
        CurrencyDetailsFragment currencyDetailsFragment = new CurrencyDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("name", name);
        bundle.putString("fullName", fullName);
        bundle.putString("iconPath", iconPath);
        currencyDetailsFragment.setArguments(bundle);
        return currencyDetailsFragment;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
        }
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        name = bundle.getString("name");
        fullName = bundle.getString("fullName");
        iconPath = bundle.getString("iconPath");
        mFragmentView = inflater.inflate(R.layout.fragment_currency_details, container, false);
        unbinder = ButterKnife.bind(this, mFragmentView);
        setupSwipeRefreshLayout();
        setupToolbar();
        setupSmartTabLayout();
        setupConversionLayout();
        setupChartLayout();
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        return mFragmentView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.fetchCurrencyPrice(name, PreferenceUtils.getConversionCurrencyChoice(getActivity()));
        mPresenter.fetchCurrencyHistogramByMinute(name, PreferenceUtils.getConversionCurrencyChoice(getActivity()), PreferenceUtils.getMarketChoice(getActivity()));
    }

    @Override
    public void onDestroyView() {
        Log.e(getClass().getSimpleName(), "onDestroyView()");
        super.onDestroyView();
        unbinder.unbind();
        mIsStopped = true;
    }

    /**
     * Sets up the toolbar.
     */
    private void setupToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mCurrencyFullName.setText(fullName);
        Glide.with(getActivity())
                .load(iconPath)
                .apply(new RequestOptions().fitCenter())
                .into(mCurrencyIcon);
    }

    /**
     * Sets up the toolbar.
     */
    private void setupSmartTabLayout() {
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string._1_d));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string._1_w));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string._1_m));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string._6_m));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string._1_y));

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        mPresenter.fetchCurrencyHistogramByMinute(name, PreferenceUtils.getConversionCurrencyChoice(getActivity()), PreferenceUtils.getMarketChoice(getActivity()));
                        break;
                    case 1:
                        mPresenter.fetchCurrencyHistogramByHour(name, PreferenceUtils.getConversionCurrencyChoice(getActivity()), PreferenceUtils.getMarketChoice(getActivity()), 168);
                        break;
                    case 2:
                        mPresenter.fetchCurrencyHistogramByHour(name, PreferenceUtils.getConversionCurrencyChoice(getActivity()), PreferenceUtils.getMarketChoice(getActivity()), 730);
                        break;
                    case 3:
                        mPresenter.fetchCurrencyHistogramByDay(name, PreferenceUtils.getConversionCurrencyChoice(getActivity()), PreferenceUtils.getMarketChoice(getActivity()), 180);
                        break;
                    case 4:
                        mPresenter.fetchCurrencyHistogramByDay(name, PreferenceUtils.getConversionCurrencyChoice(getActivity()), PreferenceUtils.getMarketChoice(getActivity()), 365);
                        break;
                    case 5:
                        mPresenter.fetchCurrencyHistogramByDay(name, PreferenceUtils.getConversionCurrencyChoice(getActivity()), PreferenceUtils.getMarketChoice(getActivity()), 365);
                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    /**
     * Sets up the swipe refresh layout.
     */
    private void setupSwipeRefreshLayout() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mPresenter.fetchCurrencyPrice(name, PreferenceUtils.getConversionCurrencyChoice(getActivity()));
                switch (mTabLayout.getSelectedTabPosition()) {
                    case 0:
                        mPresenter.fetchCurrencyHistogramByMinute(name, PreferenceUtils.getConversionCurrencyChoice(getActivity()), PreferenceUtils.getMarketChoice(getActivity()));
                        break;
                    case 1:
                        mPresenter.fetchCurrencyHistogramByHour(name, PreferenceUtils.getConversionCurrencyChoice(getActivity()), PreferenceUtils.getMarketChoice(getActivity()), 168);
                        break;
                    case 2:
                        mPresenter.fetchCurrencyHistogramByHour(name, PreferenceUtils.getConversionCurrencyChoice(getActivity()), PreferenceUtils.getMarketChoice(getActivity()), 730);
                        break;
                    case 3:
                        mPresenter.fetchCurrencyHistogramByDay(name, PreferenceUtils.getConversionCurrencyChoice(getActivity()), PreferenceUtils.getMarketChoice(getActivity()), 180);
                        break;
                    case 4:
                        mPresenter.fetchCurrencyHistogramByDay(name, PreferenceUtils.getConversionCurrencyChoice(getActivity()), PreferenceUtils.getMarketChoice(getActivity()), 365);
                        break;
                    case 5:
                        mPresenter.fetchCurrencyHistogramByDay(name, PreferenceUtils.getConversionCurrencyChoice(getActivity()), PreferenceUtils.getMarketChoice(getActivity()), 365);
                        break;
                }
            }
        });
    }

    /**
     * Sets up the edit texts for currency comparision.
     */
    private void setupConversionLayout() {
        mInvertSymbolConversionLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isConversionInverted = !isConversionInverted;
                mFromSymbolValueEditText.setText(String.valueOf(1.0));
            }
        });
        mFromSymbolValueEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    populateConversionLayout(fromSymbol, toSymbol, Double.parseDouble(editable.toString()), toSymbolRate, isConversionInverted);
                } else {
                    populateConversionLayout(fromSymbol, toSymbol, 0.0, toSymbolRate, isConversionInverted);
                }
            }
        });
    }

    /**
     * Sets up the chart layout.
     */
    private void setupChartLayout() {
        mCurrencyHistogramLineChart.getAxisLeft().setEnabled(false);
        mCurrencyHistogramLineChart.getAxisRight().setDrawLabels(true);
        mCurrencyHistogramLineChart.getAxisRight().setLabelCount(6, true);
        mCurrencyHistogramLineChart.getAxisRight().setDrawGridLines(true);
        mCurrencyHistogramLineChart.getAxisRight().setGridColor(ActivityCompat.getColor(getActivity(), R.color.colorGraphGridLine));
        mCurrencyHistogramLineChart.getAxisRight().enableGridDashedLine(4,2, 2);
        mCurrencyHistogramLineChart.getAxisRight().setDrawAxisLine(false);
        mCurrencyHistogramLineChart.getAxisRight().setTextColor(ActivityCompat.getColor(getActivity(), R.color.colorText));
        mCurrencyHistogramLineChart.getAxisRight().setTextSize(8f);
        mCurrencyHistogramLineChart.getXAxis().setDrawGridLines(false);
        mCurrencyHistogramLineChart.getXAxis().setDrawAxisLine(false);
        mCurrencyHistogramLineChart.getXAxis().setDrawLabels(true);
        mCurrencyHistogramLineChart.getXAxis().setLabelCount(6, true);
        mCurrencyHistogramLineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        mCurrencyHistogramLineChart.getXAxis().setTextColor(ActivityCompat.getColor(getActivity(), R.color.colorText));
        mCurrencyHistogramLineChart.getXAxis().setTextSize(8f);
        mCurrencyHistogramLineChart.getXAxis().setAvoidFirstLastClipping(true);
        mCurrencyHistogramLineChart.setDescription(null);
        mCurrencyHistogramLineChart.getLegend().setEnabled(false);
        mCurrencyHistogramLineChart.setTouchEnabled(false);
        mCurrencyHistogramLineChart.setPinchZoom(true);
        mCurrencyHistogramLineChart.setNoDataText("");
    }

    private void populateConversionLayout(String fromSymbol, String toSymbol, double inputValue, double conversionRate, boolean isConversionInverted) {
        if (!isConversionInverted && conversionRate != 0.0) {
            mToSymbolNameTextView.setText(toSymbol);
            mFromSymbolNameTextView.setText(fromSymbol);
            mToSymbolValueTextView.setText(String.valueOf(inputValue * toSymbolRate));
        } else if (!isConversionInverted) {
            mFromSymbolNameTextView.setText(fromSymbol);
            mToSymbolNameTextView.setText(toSymbol);
            mToSymbolValueTextView.setText(String.valueOf(0.0));
        } else if (conversionRate != 0.0) {
            mToSymbolNameTextView.setText(fromSymbol);
            mFromSymbolNameTextView.setText(toSymbol);
            mToSymbolValueTextView.setText(String.valueOf(inputValue / toSymbolRate));
        } else {
            mFromSymbolNameTextView.setText(fromSymbol);
            mToSymbolNameTextView.setText(toSymbol);
            mToSymbolValueTextView.setText(String.valueOf(0.0));
        }
    }

    @Override
    public void setPresenter(CurrencyDetailsFragmentContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showLoadingIndicator() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoadingIndicator() {
        if (!isHistogramLoading && !isPriceLoading) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void showConnectionErrorMessage() {
        mErrorLayout.setVisibility(View.VISIBLE);
        mContentNestedScrollView.setVisibility(View.GONE);
    }

    @Override
    public void isPriceLoading(boolean isPriceLoading) {
        this.isPriceLoading = isPriceLoading;
    }

    @Override
    public void isHistogramLoading(boolean isHistogramLoading) {
        this.isHistogramLoading = isHistogramLoading;
    }

    @Override
    public int getTabLayoutPosition() {
        return mTabLayout.getSelectedTabPosition();
    }

    @Override
    public void onCurrencyPriceError() {
        toSymbolRate = 0.0;
        mCurrencyToTypeTextView.setText("");
        mCurrencyRateTextView.setText("0.0");
        mCurrencyDeltaSummaryTextView.setText(String.format(getString(R.string.currency_rate_summary), "0.0", "0.0"));
        Drawable drawable = ActivityCompat.getDrawable(getActivity(), R.drawable.ic_arrow_upward_24dp);
        DrawableCompat.wrap(drawable).mutate().setTint(ActivityCompat.getColor(getActivity(), android.R.color.holo_green_dark));
        mCurrencyDeltaImageView.setImageDrawable(drawable);
        mFromSymbolNameTextView.setText("");
        mToSymbolNameTextView.setText("");
        mFromSymbolValueEditText.setText("0");
        mToSymbolValueTextView.setText("0");
        mOpen24HoursTextView.setText("0");
        mHigh24HoursTextView.setText("0");
        mLow24HoursTextView.setText("0");
        mMarketCapTextView.setText("0");
        Utils.showSnackBar(mContentNestedScrollView, getString(R.string.fetching_currency_price_error));
    }

    @Override
    public void onCurrencyHistogramError() {
        List<Entry> entryList = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            entryList.add(new Entry(0.0F, System.currentTimeMillis() - i * 60 * 1000));
        }
        mCurrencyHistogramLineChart.getAxisLeft().setAxisMinimum(-1.0f);
        mCurrencyHistogramLineChart.getAxisLeft().setAxisMaximum(10.0f);
        LineDataSet lineDataSet = new LineDataSet(entryList, "");
        lineDataSet.setColors(new int[]{R.color.colorGraphStroke}, getActivity());
        lineDataSet.setDrawCircles(false);
        lineDataSet.setLineWidth(1.5F);
        lineDataSet.setValueTypeface(ResourcesCompat.getFont(getActivity(), R.font.noto_sans));
        LineData lineData = new LineData(lineDataSet);

        mCurrencyHistogramLineChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date((long) value * 1000));
            }
        });
        mCurrencyHistogramLineChart.setData(lineData);
        mCurrencyHistogramLineChart.animateX(1000);
        Utils.showSnackBar(mContentNestedScrollView, getString(R.string.fetching_currency_histogram_error));
    }

    @Override
    public void receiveCurrencyPrice(CurrencyDetailsWrapperModel currencyDetailsWrapperModel) {
        isConversionInverted = false;
        toSymbolRate = currencyDetailsWrapperModel.getmCurrencyDetailsRawModel().getPrice();
        fromSymbol = currencyDetailsWrapperModel.getmCurrencyDetailsDisplayModel().getFromSymbol();
        toSymbol = currencyDetailsWrapperModel.getmCurrencyDetailsDisplayModel().getToSymbol();
        mCurrencyToTypeTextView.setText(currencyDetailsWrapperModel.getmCurrencyDetailsDisplayModel().getToSymbol());
        if (currencyDetailsWrapperModel.getmCurrencyDetailsRawModel().getPrice() > 1) {
            mCurrencyRateTextView.setText(NumberFormat.getNumberInstance(Locale.getDefault()).format(currencyDetailsWrapperModel.getmCurrencyDetailsRawModel().getPrice()));
        } else {
            mCurrencyRateTextView.setText(String.valueOf(currencyDetailsWrapperModel.getmCurrencyDetailsRawModel().getPrice()));
        }
        mCurrencyDeltaSummaryTextView.setText(String.format(getString(R.string.currency_rate_summary), currencyDetailsWrapperModel.getmCurrencyDetailsDisplayModel().getChange24Hour().substring(currencyDetailsWrapperModel.getmCurrencyDetailsDisplayModel().getToSymbol().length()),
                currencyDetailsWrapperModel.getmCurrencyDetailsDisplayModel().getChangePct24Hour()));
        if (currencyDetailsWrapperModel.getmCurrencyDetailsRawModel().getChange24Hour() < 0) {
            Drawable drawable = ActivityCompat.getDrawable(getActivity(), R.drawable.ic_arrow_downward_24dp);
            DrawableCompat.wrap(drawable).mutate().setTint(ActivityCompat.getColor(getActivity(), android.R.color.holo_red_dark));
            mCurrencyDeltaImageView.setImageDrawable(drawable);
        } else {
            Drawable drawable = ActivityCompat.getDrawable(getActivity(), R.drawable.ic_arrow_upward_24dp);
            DrawableCompat.wrap(drawable).mutate().setTint(ActivityCompat.getColor(getActivity(), android.R.color.holo_green_dark));
            mCurrencyDeltaImageView.setImageDrawable(drawable);
        }
        mOpen24HoursTextView.setText(currencyDetailsWrapperModel.getmCurrencyDetailsDisplayModel().getOpen24Hour());
        mMarketCapTextView.setText(currencyDetailsWrapperModel.getmCurrencyDetailsDisplayModel().getMktCap());
        mHigh24HoursTextView.setText(currencyDetailsWrapperModel.getmCurrencyDetailsDisplayModel().getHigh24Hour());
        mLow24HoursTextView.setText(currencyDetailsWrapperModel.getmCurrencyDetailsDisplayModel().getLow24Hour());
        mFromSymbolValueEditText.setText(String.valueOf(1.0));
    }

    @Override
    public void receiveCurrencyHistogram(CurrencyHistogramModel currencyHistogramModel) {
        final int tabPosition = mTabLayout.getSelectedTabPosition();
        Double minimumValue = currencyHistogramModel.getData().get(0).getClose();
        Double maximumValue = currencyHistogramModel.getData().get(0).getClose();
        List<Entry> entryList = new ArrayList<>();
        for (CurrencyHistogramModel.Datum datum : currencyHistogramModel.getData()) {
            entryList.add(new Entry(datum.getTime(), datum.getClose().floatValue()));
            if (datum.getClose() < minimumValue) {
                minimumValue = datum.getClose();
            }
            if (datum.getClose() > maximumValue) {
                maximumValue = datum.getClose();
            }
        }
        mCurrencyHistogramLineChart.getAxisRight().resetAxisMaximum();
        mCurrencyHistogramLineChart.getAxisRight().resetAxisMinimum();
        LineDataSet lineDataSet = new LineDataSet(entryList, "");
        lineDataSet.setColors(new int[]{R.color.colorGraphStroke}, getActivity());
        lineDataSet.setDrawCircles(false);
        lineDataSet.setLineWidth(1.5F);
        lineDataSet.setValueTypeface(ResourcesCompat.getFont(getActivity(), R.font.noto_sans));
        LineData lineData = new LineData(lineDataSet);

        mCurrencyHistogramLineChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                switch (tabPosition) {
                    case 0:
                        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date((long) value * 1000));
                    case 1:
                        return new SimpleDateFormat("dd MMM", Locale.getDefault()).format(new Date((long) value * 1000));
                    case 2:
                        return new SimpleDateFormat("dd MMM", Locale.getDefault()).format(new Date((long) value * 1000));
                    case 3:
                        return new SimpleDateFormat("dd MMM", Locale.getDefault()).format(new Date((long) value * 1000));
                    case 4:
                        return new SimpleDateFormat("dd MMM", Locale.getDefault()).format(new Date((long) value * 1000));
                    case 5:
                        return new SimpleDateFormat("dd MMM", Locale.getDefault()).format(new Date((long) value * 1000));
                    default:
                        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date((long) value * 1000));
                }
            }
        });
        mCurrencyHistogramLineChart.setData(lineData);
        mCurrencyHistogramLineChart.animateX(1000);
    }

    @Override
    public boolean isActive() {
        return isAdded() && !mIsStopped;
    }
}
