package com.alacritystudios.cryptotrace.ui.main.fragment.currencylist;


import com.alacritystudios.cryptotrace.models.CurrencyModel;
import com.alacritystudios.cryptotrace.models.CurrencyListWrapperModel;
import com.alacritystudios.cryptotrace.sql.CryptoWatchDatabaseHelper;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Main entry point for accessing currency list data.
 */

public interface CurrencyListFragmentDataSource {

    public interface FetchCryptoCurrencyListNetworkCallback {

        void onCurrencyListFetched(CurrencyListWrapperModel currencyListWrapperModel);

        void onDataNotAvailable();
    }

    public interface FetchCryptoCurrencyListLocalCacheCallback {

        void onCurrencyListFetched(List<CurrencyModel> currencyModelList);

        void onDataNotAvailable();
    }

    void fetchCryptoCurrencyListFromNetwork(FetchCryptoCurrencyListNetworkCallback fetchCryptoCurrencyListNetworkCallback);

    void fetchCryptoCurrencyListFromLocalCache(WeakReference<CryptoWatchDatabaseHelper> cryptoWatchDatabaseHelperWeakReference, final boolean isCryptoCurency, FetchCryptoCurrencyListLocalCacheCallback fetchCryptoCurrencyListLocalCacheCallback);
}
