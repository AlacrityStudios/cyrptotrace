package com.alacritystudios.cryptotrace.ui.main.fragment.exchange;

import com.alacritystudios.cryptotrace.models.ExchangeModel;
import com.alacritystudios.cryptotrace.ui.main.MainActivityRepository;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;

/**
 * Listens to user actions from the UI ({@link ExchangeFragment}), retrieves the data and updates the
 * UI as required.
 */

public class ExchangeFragmentPresenter implements ExchangeFragmentContract.Presenter {

    private WeakReference<ExchangeFragmentContract.View> mViewWeakReference;
    private MainActivityRepository mMainActivityRepository;

    public ExchangeFragmentPresenter(ExchangeFragment mCurrencyListFragment) {
        this.mViewWeakReference = new WeakReference<ExchangeFragmentContract.View>(mCurrencyListFragment);
        this.mMainActivityRepository = MainActivityRepository.getInstance();
        this.mViewWeakReference.get().setPresenter(this);
    }

    @Override
    public void start() {
        fetchExchangeList();
    }

    @Override
    public void fetchExchangeList() {
        mViewWeakReference.get().showLoadingIndicator();
        mMainActivityRepository.fetchExchangeList(new ExchangeFragmentDataSource.FetchExchangeListCallback() {

            @Override
            public void onExchangeListFetched(Map<String, Map<String, List<String>>> stringMapMap) {
                if (stringMapMap != null && stringMapMap.keySet().size() > 0) {
                    if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                        mViewWeakReference.get().hideLoadingIndicator();
                        mViewWeakReference.get().receiveResult(stringMapMap);
                    }
                } else {
                    if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                        mViewWeakReference.get().hideLoadingIndicator();
                        mViewWeakReference.get().showConnectionErrorMessage();
                    }
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                    mViewWeakReference.get().hideLoadingIndicator();
                    mViewWeakReference.get().showConnectionErrorMessage();
                }
            }
        });
    }
}
