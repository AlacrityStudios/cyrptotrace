package com.alacritystudios.cryptotrace.ui.main.fragment.calculator;

import com.alacritystudios.cryptotrace.models.CurrencyHistogramModel;
import com.alacritystudios.cryptotrace.ui.base.BasePresenter;
import com.alacritystudios.cryptotrace.ui.base.BaseView;

import java.util.List;

/**
 * Defines a contract between the view and the presenter.
 */

public interface CalculatorFragmentContract {

    public interface View extends BaseView<CalculatorFragmentContract.Presenter> {

        void showLoadingIndicator();

        void hideLoadingIndicator();

        void showConnectionErrorMessage();

        void receiveCurrencyList(List<String> currencyNameList);

        void receiveCurrencyPrice(CurrencyHistogramModel currencyHistogramModel);

        boolean isActive();
    }

    public interface Presenter extends BasePresenter {

        void fetchCurrencyList();

        void fetchCurrencyPrice(String fromSymbol, String toSymbol);
    }
}
