package com.alacritystudios.cryptotrace.ui.main.fragment.currencylist;

import com.alacritystudios.cryptotrace.models.CurrencyModel;
import com.alacritystudios.cryptotrace.models.CurrencyListWrapperModel;
import com.alacritystudios.cryptotrace.sql.CryptoWatchDatabaseHelper;
import com.alacritystudios.cryptotrace.ui.main.MainActivityRepository;
import com.alacritystudios.cryptotrace.ui.main.fragment.metadata.MetadataFragmentDataSource;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Listens to user actions from the UI ({@link CurrencyListFragment}), retrieves the data and updates the
 * UI as required.
 */

public class CurrencyListFragmentPresenter implements CurrencyListFragmentContract.Presenter {

    private WeakReference<CurrencyListFragmentContract.View> mViewWeakReference;
    private WeakReference<CryptoWatchDatabaseHelper> mCryptoWatchDatabaseHelperWeakReference;
    private MainActivityRepository mMainActivityRepository;

    public CurrencyListFragmentPresenter(CurrencyListFragment mCurrencyListFragment, CryptoWatchDatabaseHelper cryptoWatchDatabaseHelper) {
        this.mViewWeakReference = new WeakReference<CurrencyListFragmentContract.View>(mCurrencyListFragment);
        this.mCryptoWatchDatabaseHelperWeakReference = new WeakReference<CryptoWatchDatabaseHelper>(cryptoWatchDatabaseHelper);
        this.mMainActivityRepository = MainActivityRepository.getInstance();
        this.mViewWeakReference.get().setPresenter(this);
    }

    @Override
    public void start() {
        fetchCurrencyList();
    }

    @Override
    public void fetchCurrencyList() {
        mViewWeakReference.get().showLoadingIndicator();
        if (System.currentTimeMillis() - mViewWeakReference.get().fetchLastCurrencyUpdateDate() > TimeUnit.HOURS.toMillis(24)) {
            mMainActivityRepository.fetchCryptoCurrencyListFromNetwork(new CurrencyListFragmentDataSource.FetchCryptoCurrencyListNetworkCallback() {
                @Override
                public void onCurrencyListFetched(CurrencyListWrapperModel currencyListWrapperModel) {
                    if (currencyListWrapperModel != null && currencyListWrapperModel.getData() != null) {
                        mMainActivityRepository.saveCurrencyListIntoLocalCache(currencyListWrapperModel, mCryptoWatchDatabaseHelperWeakReference, true, new MetadataFragmentDataSource.SaveCurrencyListIntoLocalCacheCallback() {
                            @Override
                            public void onSuccess() {
                                if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                                    mViewWeakReference.get().saveCurrencyUpdateDate();
                                }
                            }

                            @Override
                            public void onFailure() {
                            }
                        });

                        if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                            mViewWeakReference.get().hideLoadingIndicator();
                            mViewWeakReference.get().receiveResult(new ArrayList<CurrencyModel>(currencyListWrapperModel.getData().values()));
                        }
                    } else {
                        if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                            mViewWeakReference.get().hideLoadingIndicator();
                            mViewWeakReference.get().showPlaceholderMessage();
                        }
                    }
                }

                @Override
                public void onDataNotAvailable() {
                    if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                        mViewWeakReference.get().hideLoadingIndicator();
                        mViewWeakReference.get().showConnectionErrorMessage();
                    }
                }
            });
        } else {
            mMainActivityRepository.fetchCryptoCurrencyListFromLocalCache(mCryptoWatchDatabaseHelperWeakReference, true, new CurrencyListFragmentDataSource.FetchCryptoCurrencyListLocalCacheCallback() {
                @Override
                public void onCurrencyListFetched(List<CurrencyModel> currencyModelList) {
                    if (currencyModelList != null) {
                        if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                            mViewWeakReference.get().receiveResult(currencyModelList);
                            mViewWeakReference.get().hideLoadingIndicator();
                        }
                    } else {
                        if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                            mViewWeakReference.get().hideLoadingIndicator();
                            mViewWeakReference.get().showPlaceholderMessage();
                        }
                    }
                }

                @Override
                public void onDataNotAvailable() {
                    if (mViewWeakReference.get() != null && mViewWeakReference.get().isActive()) {
                        mViewWeakReference.get().hideLoadingIndicator();
                        mViewWeakReference.get().showConnectionErrorMessage();
                    }
                }
            });
        }
    }
}
