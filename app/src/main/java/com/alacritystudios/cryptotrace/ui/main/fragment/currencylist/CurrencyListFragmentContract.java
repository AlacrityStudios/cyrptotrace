package com.alacritystudios.cryptotrace.ui.main.fragment.currencylist;

import com.alacritystudios.cryptotrace.models.CurrencyModel;
import com.alacritystudios.cryptotrace.ui.base.BasePresenter;
import com.alacritystudios.cryptotrace.ui.base.BaseView;

import java.util.List;

/**
 * Defines a contract between the view and the presenter.
 */

public interface CurrencyListFragmentContract {

    public interface View extends BaseView<Presenter> {

        void showLoadingIndicator();

        void hideLoadingIndicator();

        void showConnectionErrorMessage();

        void showPlaceholderMessage();

        long fetchLastCurrencyUpdateDate();

        void saveCurrencyUpdateDate();

        void receiveResult(List<CurrencyModel> currencyModelList);

        boolean isActive();
    }

    public interface Presenter extends BasePresenter {

        void fetchCurrencyList();
    }
}
