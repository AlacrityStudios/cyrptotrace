package com.alacritystudios.cryptotrace.ui.main.fragment.toolbar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapters.ViewPagerAdapter;
import com.alacritystudios.cryptotrace.ui.main.fragment.exchange.ExchangeFragment;
import com.alacritystudios.cryptotrace.ui.main.fragment.exchange.ExchangeFragmentPresenter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Fragment to contain the toolbar.
 */

public class ToolbarFragment extends Fragment {

    View mFragmentView;
    @BindView(R.id.av_exchanges_list)
    AdView mAdView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    @BindView(R.id.smart_tab_layout)
    SmartTabLayout mSmartTabLayout;


    ViewPagerAdapter mViewPagerAdapter;

    Unbinder unbinder;

    public static ToolbarFragment getInstance() {
        return new ToolbarFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(getClass().getSimpleName(), "onCreateView()");
        mFragmentView = inflater.inflate(R.layout.fragment_toolbar, container, false);
        unbinder = ButterKnife.bind(this, mFragmentView);
        setupToolbar();
        setupAdView();
        setupViewPager();
        return mFragmentView;
    }

    /**
     * Sets up the toolbar.
     */
    private void setupToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
    }

    /**
     * Sets up the toolbar.
     */
    private void setupAdView() {
        AdRequest adRequest = new AdRequest.Builder().build();
        //mAdView.loadAd(adRequest);
    }

    /**
     * Sets up the view pager.
     */
    private void setupViewPager() {
        Log.i(getClass().getSimpleName(), "setupViewPager()");
        mViewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mViewPagerAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mViewPagerAdapter.getCount() == 0) {
            ExchangeFragment exchangeFragment = ExchangeFragment.getInstance();
            new ExchangeFragmentPresenter(exchangeFragment);
            mViewPagerAdapter.addFragment(exchangeFragment, getString(R.string.tab_layout_header_exchanges));
            mViewPagerAdapter.addFragment(new Fragment(), "Currencies");
            mViewPagerAdapter.notifyDataSetChanged();
            mSmartTabLayout.setViewPager(mViewPager);
        } else {
            ExchangeFragment exchangeFragment = (ExchangeFragment) mViewPagerAdapter.getItem(0);
            new ExchangeFragmentPresenter(exchangeFragment);
        }
    }

    @Override
    public void onDestroyView() {
        Log.i(getClass().getSimpleName(), "onDestroyView()");
        super.onDestroyView();
        unbinder.unbind();
    }
}
