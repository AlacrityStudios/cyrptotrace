package com.alacritystudios.cryptotrace.ui.base;

public interface BasePresenter {

    void start();

}
