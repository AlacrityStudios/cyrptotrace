package com.alacritystudios.cryptotrace.ui.main.fragment.calculator;

import com.alacritystudios.cryptotrace.models.CurrencyDetailsWrapperModel;

/**
 * Main entry point for accessing currency conversion data.
 */

public interface CalculatorFragmentDataSource {

    public interface CurrencyDetailsCallback {

        void onCurrencyDetailsFetched(CurrencyDetailsWrapperModel currencyDetailsWrapperModel);

        void onDataNotAvailable();
    }
}
