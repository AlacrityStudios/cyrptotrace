package com.alacritystudios.cryptotrace.ui.main.fragment.marketlistdialog;

import com.alacritystudios.cryptotrace.models.MarketModel;
import com.alacritystudios.cryptotrace.sql.CryptoWatchDatabaseHelper;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Main entry point for accessing currency list data.
 */

public interface MarketListDialogFragmentDataSource {

    public interface FetchMarketListLocalCacheCallback {

        void onMarketListFetched(List<MarketModel> marketModelList);

        void onDataNotAvailable();
    }

    void fetchMarketListFromLocalCache(WeakReference<CryptoWatchDatabaseHelper> cryptoWatchDatabaseHelperWeakReference,
                                       MarketListDialogFragmentDataSource.FetchMarketListLocalCacheCallback fetchMarketListLocalCacheCallback);
}
