package com.alacritystudios.cryptotrace.ui.main.fragment.currencylistdialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.adapters.AllCurrencyListAdapter;
import com.alacritystudios.cryptotrace.models.CurrencyModel;
import com.alacritystudios.cryptotrace.utilities.PreferenceUtils;
import com.lapism.searchview.SearchView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Returns a dialog to select the currency.
 */

public class CurrencyListDialogFragment extends DialogFragment implements CurrencyListDialogFragmentContract.View, AllCurrencyListAdapter.CoinsListAdapterListener {

    View mFragmentView;
    @BindView(R.id.sv_coins_list)
    SearchView mSearchView;
    @BindView(R.id.tl_currency_types)
    TabLayout mTabLayout;
    @BindView(R.id.rv_currency_list)
    RecyclerView mRecyclerView;

    Unbinder unbinder;
    boolean mIsStopped = false;
    List<CurrencyModel> mAllCurrencyModels;
    List<CurrencyModel> mSearchedCurrencyModels;
    CurrencyListDialogFragmentContract.Presenter mPresenter;
    AllCurrencyListAdapter mAllCurrencyListAdapter;
    LinearLayoutManager mLinearLayoutManager;

    CurrencyListDialogFragmentInterface mCurrencyListDialogFragmentInterface;

    public static CurrencyListDialogFragment getInstance() {
        return new CurrencyListDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mFragmentView = inflater.inflate(R.layout.dialog_currency_list, container, false);
        unbinder = ButterKnife.bind(this, mFragmentView);
        setupRecyclerView();
        setupSearchView();
        setupSmartTabLayout();
        return mFragmentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            if (getTargetFragment() != null) {
                mCurrencyListDialogFragmentInterface = (CurrencyListDialogFragmentInterface) getTargetFragment();
            } else {
                mCurrencyListDialogFragmentInterface = (CurrencyListDialogFragmentInterface) context;
            }
        } catch (ClassCastException e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCurrencyListDialogFragmentInterface = null;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mTabLayout.getSelectedTabPosition() == 0) {
            mPresenter.fetchCurrencyList(false);
        } else {
            mPresenter.fetchCurrencyList(true);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        mIsStopped = true;
    }

    @Override
    public void setPresenter(CurrencyListDialogFragmentContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void receiveResult(List<CurrencyModel> currencyModelList) {
        mAllCurrencyModels = currencyModelList;
        mSearchedCurrencyModels = new ArrayList<>();
        if (mSearchView.getTextOnly().length() > 0) {
            for (CurrencyModel currencyModel : currencyModelList) {
                if (currencyModel.getCoinName().toLowerCase().contains(mSearchView.getTextOnly().toString().toLowerCase())) {
                    mSearchedCurrencyModels.add(currencyModel);
                }
            }
        } else {
            mSearchedCurrencyModels = mAllCurrencyModels;
        }
        mAllCurrencyListAdapter.setmCurrencyModels(mSearchedCurrencyModels);
        mAllCurrencyListAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean isActive() {
        return isAdded() && !mIsStopped;
    }

    @Override
    public void onItemClick(String name, String fullName) {
        mCurrencyListDialogFragmentInterface.onCurrencyListItemClick(name);
    }

    /**
     * Sets up the search view.
     */
    private void setupSearchView() {
        LinearLayout linearLayout = mSearchView.findViewById(com.lapism.searchview.R.id.search_linearLayout);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics())));
        linearLayout.setPadding((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics()), 0, 0, 0);
        mSearchView.findViewById(com.lapism.searchview.R.id.search_imageView_arrow).setVisibility(View.GONE);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                mSearchedCurrencyModels = new ArrayList<>();
                if (mAllCurrencyModels != null && mAllCurrencyModels.size() > 0) {
                    for (CurrencyModel currencyModel : mAllCurrencyModels) {
                        if (currencyModel.getFullName().toLowerCase().contains(mSearchView.getTextOnly().toString().toLowerCase())) {
                            mSearchedCurrencyModels.add(currencyModel);
                        }
                    }
                }
                mAllCurrencyListAdapter.setmCurrencyModels(mSearchedCurrencyModels);
                mAllCurrencyListAdapter.notifyDataSetChanged();
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
        });
    }

    /**
     * Sets up the toolbar.
     */
    private void setupSmartTabLayout() {
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.local));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.crypto));
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    mPresenter.fetchCurrencyList(false);
                } else {
                    mPresenter.fetchCurrencyList(true);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    /**
     * Initialises the recycler view and attaches a layout manager and an adapter to it to display data.
     */
    private void setupRecyclerView() {
        mAllCurrencyListAdapter = new AllCurrencyListAdapter(getActivity(), new ArrayList<CurrencyModel>(),
                PreferenceUtils.getConversionCurrencyChoice(getActivity()), this);
        mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setAdapter(mAllCurrencyListAdapter);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
    }

    public interface CurrencyListDialogFragmentInterface {

        void onCurrencyListItemClick(String currencyShortName);
    }
}
