package com.alacritystudios.cryptotrace.utilities;

import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * A common place for all utility methods.
 */

public class Utils {

    public static final String CRYPTO_COMPARE_BASE_URL = "https://www.cryptocompare.com/api/data/";
    public static final String CRYPTO_COMPARE_MINI_API_BASE_URL = "https://min-api.cryptocompare.com/data/";
    public static final String CRYPTO_COMPARE_MINI_API_MEDIA_URL = "https://www.cryptocompare.com";
    public static final String FIXER_IO_BASE_URL = "http://api.fixer.io/";

    public static String getAppendedMediaUrl(String mediaUrl) {
        return CRYPTO_COMPARE_MINI_API_MEDIA_URL + mediaUrl;
    }

    public static void showSnackBar(View view, String message) {
        Snackbar.make(view, message, 3000).show();
    }
}
