package com.alacritystudios.cryptotrace.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * A utility for updating and fetching preferences.
 */

public class PreferenceUtils {

    private static final String PREF_LOCAL_CURRENCY_UPDATE_DATE = "pref_local_currency_update_date";
    private static final String PREF_CRYPTO_CURRENCY_UPDATE_DATE = "pref_crypto_currency_update_date";
    private static final String PREF_MARKET_UPDATE_DATE = "pref_market_update_date";
    private static final String PREF_CONVERSION_CURRENCY_CHOICE = "pref_conversion_currency_choice";
    private static final String PREF_MARKET_CHOICE = "pref_market_choice";

    public static void setLastCryptoCurrencyUpdateDate(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putLong(PREF_CRYPTO_CURRENCY_UPDATE_DATE, System.currentTimeMillis()).apply();
    }

    public static long getLastCryptoCurrencyUpdateDate(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getLong(PREF_CRYPTO_CURRENCY_UPDATE_DATE, 0L);
    }

    public static void setLastLocalCurrencyUpdateDate(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putLong(PREF_LOCAL_CURRENCY_UPDATE_DATE, System.currentTimeMillis()).apply();
    }

    public static long getLastLocalCurrencyUpdateDate(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getLong(PREF_LOCAL_CURRENCY_UPDATE_DATE, 0L);
    }

    public static void setLastMarketUpdateDate(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putLong(PREF_MARKET_UPDATE_DATE, System.currentTimeMillis()).apply();
    }

    public static long getLastMarketUpdateDate(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getLong(PREF_MARKET_UPDATE_DATE, 0L);
    }

    public static void setConversionCurrencyChoice(Context mContext, String name) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putString(PREF_CONVERSION_CURRENCY_CHOICE, name).apply();
    }

    public static String getConversionCurrencyChoice(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getString(PREF_CONVERSION_CURRENCY_CHOICE, "USD");
    }

    public static void setMarketChoice(Context mContext, String name) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        sharedPreferences.edit().putString(PREF_MARKET_CHOICE, name).apply();
    }

    public static String getMarketChoice(Context mContext) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return sharedPreferences.getString(PREF_MARKET_CHOICE, "CCCAGG");
    }
}
