package com.alacritystudios.cryptotrace.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.alacritystudios.cryptotrace.models.CurrencyModel;
import com.alacritystudios.cryptotrace.models.MarketModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A contract which defines the schema of various Tables used in the Encore system.
 */

public class CryptoWatchDatabaseHelper extends SQLiteOpenHelper {

    private static CryptoWatchDatabaseHelper mInstance = null;
    private static final String TAG = CryptoWatchDatabaseHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "CryptoWatchDatabase.db";

    private static final String SQL_CREATE_CURRENCY_TABLE =
            "CREATE TABLE " + CryptoWatchDatabaseContracts.CurrencyEntry.TABLE_NAME + " (" +
                    CryptoWatchDatabaseContracts.CurrencyEntry._ID + " INTEGER PRIMARY KEY," +
                    CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_CURRENCY_NAME + " TEXT," +
                    CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_CURRENCY_COIN_NAME + " TEXT," +
                    CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_CURRENCY_FULL_NAME + " TEXT," +
                    CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_CURRENCY_IMAGE_URL + " TEXT," +
                    CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_CURRENCY_SORT_ORDER + " INTEGER," +
                    CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_IS_CRYPTO_CURRENCY + " INTEGER)";

    private static final String SQL_CREATE_MARKET_TABLE =
            "CREATE TABLE " + CryptoWatchDatabaseContracts.MarketEntry.TABLE_NAME + " (" +
                    CryptoWatchDatabaseContracts.MarketEntry._ID + " INTEGER PRIMARY KEY," +
                    CryptoWatchDatabaseContracts.MarketEntry.COLUMN_NAME_MARKET_NAME + " TEXT," +
                    CryptoWatchDatabaseContracts.MarketEntry.COLUMN_NAME_MARKET_CODE + " TEXT)";

    public CryptoWatchDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static CryptoWatchDatabaseHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new CryptoWatchDatabaseHelper(context.getApplicationContext());
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_CURRENCY_TABLE);
        db.execSQL(SQL_CREATE_MARKET_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void saveCurrencyDetails(List<CurrencyModel> currencyModelList, boolean isCryptoCurrency) {
        Log.d(TAG, "saveCurrencyDetails() - for currency list of size: " + currencyModelList.size());
        SQLiteDatabase sqLiteWritableDatabase = this.getWritableDatabase();
        String whereClause = CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_IS_CRYPTO_CURRENCY +" =?";
        String [] whereArgs = { String.valueOf(isCryptoCurrency? 1: 0) };
        sqLiteWritableDatabase.delete(CryptoWatchDatabaseContracts.CurrencyEntry.TABLE_NAME, whereClause, whereArgs);
        sqLiteWritableDatabase.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            for (CurrencyModel currencyModel : currencyModelList) {
                contentValues.put(CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_CURRENCY_NAME, currencyModel.getName());
                contentValues.put(CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_CURRENCY_COIN_NAME, currencyModel.getCoinName());
                contentValues.put(CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_CURRENCY_FULL_NAME, currencyModel.getFullName());
                contentValues.put(CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_CURRENCY_IMAGE_URL, currencyModel.getImageUrl());
                contentValues.put(CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_CURRENCY_SORT_ORDER, Integer.parseInt(currencyModel.getSortOrder()));
                contentValues.put(CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_IS_CRYPTO_CURRENCY, (isCryptoCurrency? 1: 0));
                sqLiteWritableDatabase.insert(CryptoWatchDatabaseContracts.CurrencyEntry.TABLE_NAME, null, contentValues);
            }
            sqLiteWritableDatabase.setTransactionSuccessful();
        } finally {
            sqLiteWritableDatabase.endTransaction();
        }
        sqLiteWritableDatabase.close();
        Log.d(TAG, "saveCurrencyDetails() - successful for currency list of size: " + currencyModelList.size());
    }

    public List<CurrencyModel> fetchCurrencyDetails(boolean isCryptoCurrency) {
        Log.d(TAG, "fetchCurrencyDetails()");
        List<CurrencyModel> mCurrencyModelList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String[] columns = {
                CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_CURRENCY_NAME,
                CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_CURRENCY_COIN_NAME,
                CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_CURRENCY_FULL_NAME,
                CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_CURRENCY_IMAGE_URL,
                CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_CURRENCY_SORT_ORDER,
                CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_IS_CRYPTO_CURRENCY
        };
        String whereClause = CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_IS_CRYPTO_CURRENCY +" =?";
        String [] whereArgs = { String.valueOf(isCryptoCurrency? 1: 0) };

        Cursor cursor = sqLiteDatabase.query(CryptoWatchDatabaseContracts.CurrencyEntry.TABLE_NAME, columns, whereClause,
                whereArgs, null, null, CryptoWatchDatabaseContracts.CurrencyEntry.COLUMN_NAME_CURRENCY_SORT_ORDER + " ASC");

        if (cursor != null && cursor.moveToFirst()) {
            do {
                CurrencyModel currencyModel = new CurrencyModel();
                currencyModel.setName(cursor.getString(0));
                currencyModel.setCoinName(cursor.getString(1));
                currencyModel.setFullName(cursor.getString(2));
                currencyModel.setImageUrl(cursor.getString(3));
                currencyModel.setSortOrder(String.valueOf(cursor.getInt(4)));
                currencyModel.setCryptoCurrency(cursor.getInt(5) == 1);
                mCurrencyModelList.add(currencyModel);
            } while (cursor.moveToNext());
            cursor.close();
        }
        sqLiteDatabase.close();
        return mCurrencyModelList;
    }

    public void saveMarketDetails(List<MarketModel> marketModelList) {
        Log.d(TAG, "saveMarketDetails() - for market list of size: " + marketModelList.size());
        SQLiteDatabase sqLiteWritableDatabase = this.getWritableDatabase();
        sqLiteWritableDatabase.delete(CryptoWatchDatabaseContracts.MarketEntry.TABLE_NAME, null, null);
        sqLiteWritableDatabase.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            for (MarketModel marketModel : marketModelList) {
                contentValues.put(CryptoWatchDatabaseContracts.MarketEntry.COLUMN_NAME_MARKET_NAME, marketModel.getMarketName());
                contentValues.put(CryptoWatchDatabaseContracts.MarketEntry.COLUMN_NAME_MARKET_CODE, marketModel.getMarketCode());
                sqLiteWritableDatabase.insert(CryptoWatchDatabaseContracts.MarketEntry.TABLE_NAME, null, contentValues);
            }
            sqLiteWritableDatabase.setTransactionSuccessful();
        } finally {
            sqLiteWritableDatabase.endTransaction();
        }
        sqLiteWritableDatabase.close();
        Log.d(TAG, "saveMarketDetails() - successful for market list of size: " + marketModelList.size());
    }

    public List<MarketModel> fetchMarketDetails() {
        Log.d(TAG, "fetchMarketDetails()");
        List<MarketModel> marketModelList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String[] columns = {
                CryptoWatchDatabaseContracts.MarketEntry.COLUMN_NAME_MARKET_NAME,
                CryptoWatchDatabaseContracts.MarketEntry.COLUMN_NAME_MARKET_CODE
        };

        Cursor cursor = sqLiteDatabase.query(CryptoWatchDatabaseContracts.MarketEntry.TABLE_NAME, columns, null,
                null, null, null, CryptoWatchDatabaseContracts.MarketEntry.COLUMN_NAME_MARKET_NAME + " ASC");

        if (cursor != null && cursor.moveToFirst()) {
            do {
                MarketModel marketModel = new MarketModel(cursor.getString(0), cursor.getString(1));
                marketModelList.add(marketModel);
            } while (cursor.moveToNext());
            cursor.close();
        }
        sqLiteDatabase.close();
        return marketModelList;
    }
}
