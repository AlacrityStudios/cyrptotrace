package com.alacritystudios.cryptotrace.sql;

import android.provider.BaseColumns;

/**
 * Class to define the various table contracts.
 */

public abstract class CryptoWatchDatabaseContracts {

//    public static class CurrencyEntry implements BaseColumns {
//
//        public static final String TABLE_NAME = "currency_table";
//        public static final String COLUMN_NAME_CURRENCY_ID = "currency_id";
//        public static final String COLUMN_NAME_CURRENCY_URL = "currency_url";
//        public static final String COLUMN_NAME_CURRENCY_NAME = "currency_name";
//        public static final String COLUMN_NAME_CURRENCY_COIN_NAME = "currency_coin_name";
//        public static final String COLUMN_NAME_CURRENCY_FULL_NAME = "currency_full_name";
//        public static final String COLUMN_NAME_CURRENCY_IMAGE_URL = "currency_image_url";
//        public static final String COLUMN_NAME_CURRENCY_ALGORITHM = "currency_algorithm";
//        public static final String COLUMN_NAME_CURRENCY_PROOF_TYPE = "currency_proof_type";
//        public static final String COLUMN_NAME_CURRENCY_FULLY_PRE_MINED = "currency_fully_pre_mined";
//        public static final String COLUMN_NAME_CURRENCY_TOTAL_COIN_SUPPLY = "currency_total_coin_supply";
//        public static final String COLUMN_NAME_CURRENCY_PRE_MINED_VALUE = "currency_pre_mined_value";
//        public static final String COLUMN_NAME_CURRENCY_TOTAL_COINS_FREE_FLOAT = "currency_total_coins_free_float";
//        public static final String COLUMN_NAME_CURRENCY_SORT_ORDER = "currency_sort_ordered";
//        public static final String COLUMN_NAME_CURRENCY_SPONSORED = "currency_sponsored";
//        public static final String COLUMN_NAME_IS_CRYPTO_CURRENCY = "is_crypto_currency";
//    }

    public static class CurrencyEntry implements BaseColumns {

        public static final String TABLE_NAME = "currency_table";
        public static final String COLUMN_NAME_CURRENCY_NAME = "currency_name";
        public static final String COLUMN_NAME_CURRENCY_COIN_NAME = "currency_coin_name";
        public static final String COLUMN_NAME_CURRENCY_FULL_NAME = "currency_full_name";
        public static final String COLUMN_NAME_CURRENCY_IMAGE_URL = "currency_image_url";
        public static final String COLUMN_NAME_CURRENCY_SORT_ORDER = "currency_sort_ordered";
        public static final String COLUMN_NAME_IS_CRYPTO_CURRENCY = "is_crypto_currency";
    }

    public static class MarketEntry implements BaseColumns {

        public static final String TABLE_NAME = "market_table";
        public static final String COLUMN_NAME_MARKET_NAME = "market_name";
        public static final String COLUMN_NAME_MARKET_CODE = "market_code";
    }
}