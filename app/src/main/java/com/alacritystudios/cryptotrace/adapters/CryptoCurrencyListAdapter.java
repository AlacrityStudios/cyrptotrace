package com.alacritystudios.cryptotrace.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.models.CurrencyModel;
import com.alacritystudios.cryptotrace.utilities.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

/**
 * An adapter to display the list of crypto-currencies.
 */

public class CryptoCurrencyListAdapter extends RecyclerView.Adapter<CryptoCurrencyListAdapter.ItemViewHolder> {

    private Context mContext;
    private List<CurrencyModel> mCurrencyModels;
    private CoinsListAdapterListener mCoinsListAdapterListener;

    public CryptoCurrencyListAdapter(Context mContext, List<CurrencyModel> mCurrencyModels, CoinsListAdapterListener mCoinsListAdapterListener) {
        this.mContext = mContext;
        this.mCurrencyModels = mCurrencyModels;
        this.mCoinsListAdapterListener = mCoinsListAdapterListener;
    }

    public void setmCurrencyModels(List<CurrencyModel> mCurrencyModels) {
        this.mCurrencyModels = mCurrencyModels;
    }

    protected class ItemViewHolder extends RecyclerView.ViewHolder {

        View mRootView;
        ImageView mCoinLogoIV;
        TextView mCoinNameTV;
        Animation mAnimation;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootView = itemView;
            mCoinLogoIV = itemView.findViewById(R.id.iv_coin_logo);
            mCoinNameTV = itemView.findViewById(R.id.tv_coin_name);
        }

        public void clearAnimation() {
            mRootView.clearAnimation();
        }

        private void setScaleAnimation() {
            AlphaAnimation anim = new AlphaAnimation(0.0F, 1.0F);
            anim.setDuration(300);
            mRootView.startAnimation(anim);
        }

    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_crypto_currency_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        holder.mCoinNameTV.setText(mCurrencyModels.get(position).getFullName());
        Glide.with(mContext)
                .load(Utils.getAppendedMediaUrl(mCurrencyModels.get(position).getImageUrl()))
                .apply(RequestOptions.centerCropTransform())
                .into(holder.mCoinLogoIV);
        holder.mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCoinsListAdapterListener.onItemClick(mCurrencyModels.get(position).getName(), mCurrencyModels.get(position).getFullName(), Utils.getAppendedMediaUrl(mCurrencyModels.get(position).getImageUrl()));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mCurrencyModels != null) {
            return mCurrencyModels.size();
        } else {
            return 0;
        }
    }

    @Override
    public void onViewDetachedFromWindow(ItemViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        //holder.clearAnimation();
    }

    @Override
    public void onViewAttachedToWindow(ItemViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        //holder.setScaleAnimation();
    }

    public interface CoinsListAdapterListener {
        void onItemClick(String name, String fullName, String iconPath);
    }
}
