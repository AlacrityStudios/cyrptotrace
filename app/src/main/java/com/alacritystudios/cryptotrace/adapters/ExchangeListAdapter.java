package com.alacritystudios.cryptotrace.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.alacritystudios.cryptotrace.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An adapter to display the list of all exchanges.
 */

public class ExchangeListAdapter extends RecyclerView.Adapter<ExchangeListAdapter.ItemViewHolder> {

    private Context mContext;
    private List<String> mExchanges;
    private ExchangeListAdapterListener mCoinsListAdapterListener;

    public ExchangeListAdapter(Context mContext, List<String> mExchanges, ExchangeListAdapterListener mCoinsListAdapterListener) {
        this.mContext = mContext;
        this.mExchanges = mExchanges;
        this.mCoinsListAdapterListener = mCoinsListAdapterListener;
    }

    public void setmExchanges(List<String> mExchanges) {
        this.mExchanges = mExchanges;
    }

    protected class ItemViewHolder extends RecyclerView.ViewHolder {

        View mRootView;
        @BindView(R.id.tv_exchange_name)
        TextView mExchangeNameTV;
        Animation mAnimation;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootView = itemView;
            ButterKnife.bind(this, itemView);
        }

        public void clearAnimation() {
            mRootView.clearAnimation();
        }

        private void setAlphaAnimation() {
            AlphaAnimation anim = new AlphaAnimation(0.0F, 1.0F);
            anim.setDuration(300);
            mRootView.startAnimation(anim);
        }
    }

    @Override
    public ExchangeListAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ExchangeListAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_exchange_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ExchangeListAdapter.ItemViewHolder holder, final int position) {
        holder.mExchangeNameTV.setText(mExchanges.get(position));
        holder.mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCoinsListAdapterListener.onItemClick(mExchanges.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mExchanges != null) {
            return mExchanges.size();
        } else {
            return 0;
        }
    }

    @Override
    public void onViewDetachedFromWindow(ExchangeListAdapter.ItemViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.clearAnimation();
    }

    @Override
    public void onViewAttachedToWindow(ExchangeListAdapter.ItemViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.setAlphaAnimation();
    }

    public interface ExchangeListAdapterListener {
        void onItemClick(String name);
    }
}
