package com.alacritystudios.cryptotrace.adapters;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.models.MarketModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An adapter to display the list of all available markets.
 */

public class MarketListAdapter extends RecyclerView.Adapter<MarketListAdapter.ItemViewHolder> {

    private Context mContext;
    private List<MarketModel> mMarketModelList;
    private String mDefaultValue;
    private MarketListAdapter.MarketListAdapterListener mMarketListAdapterListener;

    public MarketListAdapter(Context mContext, List<MarketModel> mMarketModelList, String mDefaultValue, MarketListAdapterListener mMarketListAdapterListener) {
        this.mContext = mContext;
        this.mMarketModelList = mMarketModelList;
        this.mDefaultValue = mDefaultValue;
        this.mMarketListAdapterListener = mMarketListAdapterListener;
    }

    public void setmMarketModelList(List<MarketModel> mMarketModelList) {
        this.mMarketModelList = mMarketModelList;
    }

    protected class ItemViewHolder extends RecyclerView.ViewHolder {

        View mRootView;
        @BindView(R.id.tv_market_name)
        TextView mMarketNameTV;
        Animation mAnimation;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootView = itemView;
            ButterKnife.bind(this, itemView);
        }

        public void clearAnimation() {
            mRootView.clearAnimation();
        }

        private void setScaleAnimation() {
            AlphaAnimation anim = new AlphaAnimation(0.0F, 1.0F);
            anim.setDuration(300);
            mRootView.startAnimation(anim);
        }

    }

    @Override
    public MarketListAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MarketListAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_market_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MarketListAdapter.ItemViewHolder holder, final int position) {
        holder.mMarketNameTV.setText(mMarketModelList.get(position).getMarketName());
        if (mDefaultValue.equals(mMarketModelList.get(position).getMarketCode())) {
            holder.mMarketNameTV.setTextColor(ActivityCompat.getColor(mContext, R.color.colorAccent));
        } else {
            holder.mMarketNameTV.setTextColor(ActivityCompat.getColor(mContext, R.color.colorFocusText));
        }
        holder.mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMarketListAdapterListener.onItemClick(mMarketModelList.get(position).getMarketCode());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mMarketModelList != null) {
            return mMarketModelList.size();
        } else {
            return 0;
        }
    }

    @Override
    public void onViewDetachedFromWindow(MarketListAdapter.ItemViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.clearAnimation();
    }

    @Override
    public void onViewAttachedToWindow(MarketListAdapter.ItemViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.setScaleAnimation();
    }

    public interface MarketListAdapterListener {
        void onItemClick(String marketCode);
    }
}