package com.alacritystudios.cryptotrace.adapters;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

import com.alacritystudios.cryptotrace.R;
import com.alacritystudios.cryptotrace.models.CurrencyModel;

import java.util.List;

/**
 * An adapter to display the list of all available currencies.
 */

public class AllCurrencyListAdapter extends RecyclerView.Adapter<AllCurrencyListAdapter.ItemViewHolder> {

    private Context mContext;
    private List<CurrencyModel> mCurrencyModels;
    private String mDefaultValue;
    private AllCurrencyListAdapter.CoinsListAdapterListener mCoinsListAdapterListener;

    public AllCurrencyListAdapter(Context mContext, List<CurrencyModel> mCurrencyModels, String mDefaultValue, CoinsListAdapterListener mCoinsListAdapterListener) {
        this.mContext = mContext;
        this.mCurrencyModels = mCurrencyModels;
        this.mDefaultValue = mDefaultValue;
        this.mCoinsListAdapterListener = mCoinsListAdapterListener;
    }

    public void setmCurrencyModels(List<CurrencyModel> mCurrencyModels) {
        this.mCurrencyModels = mCurrencyModels;
    }

    protected class ItemViewHolder extends RecyclerView.ViewHolder {

        View mRootView;
        TextView mCoinNameTV;
        Animation mAnimation;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mRootView = itemView;
            mCoinNameTV = itemView.findViewById(R.id.tv_coin_name);
        }

        public void clearAnimation() {
            mRootView.clearAnimation();
        }

        private void setScaleAnimation() {
            AlphaAnimation anim = new AlphaAnimation(0.0F, 1.0F);
            anim.setDuration(300);
            mRootView.startAnimation(anim);
        }

    }

    @Override
    public AllCurrencyListAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AllCurrencyListAdapter.ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_view_all_currency_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(AllCurrencyListAdapter.ItemViewHolder holder, final int position) {
        holder.mCoinNameTV.setText(mCurrencyModels.get(position).getFullName());
        if (mDefaultValue.equals(mCurrencyModels.get(position).getName())) {
            holder.mCoinNameTV.setTextColor(ActivityCompat.getColor(mContext, R.color.colorAccent));
        } else {
            holder.mCoinNameTV.setTextColor(ActivityCompat.getColor(mContext, R.color.colorFocusText));
        }
        holder.mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCoinsListAdapterListener.onItemClick(mCurrencyModels.get(position).getName(), mCurrencyModels.get(position).getFullName());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mCurrencyModels != null) {
            return mCurrencyModels.size();
        } else {
            return 0;
        }
    }

    @Override
    public void onViewDetachedFromWindow(AllCurrencyListAdapter.ItemViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.clearAnimation();
    }

    @Override
    public void onViewAttachedToWindow(AllCurrencyListAdapter.ItemViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.setScaleAnimation();
    }

    public interface CoinsListAdapterListener {
        void onItemClick(String name, String fullName);
    }
}